const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
    packageType: {
        type: String,
        required: true
    },
    deliveryDays: {
        type: Number,
        required: true
    },
    amount: {
        type: String,
        required: true
    }
})

const model = mongoose.model('ShippingRate', schema)

module.exports = model