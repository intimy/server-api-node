const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
    paypalEmail: String,
    userId: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    userIp: {
        type: String,
        required: true
    },
    verified: {
        type: Boolean,
        default: false
    },
    validated: Boolean,
    reason: String,
    date: {
        type: Date,
        default: Date.now
    }
})

const model = mongoose.model('WithdrawRequest', schema)

module.exports = model