const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  sellerId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  name: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  description: String,
  price: {
    type: mongoose.Schema.Types.Number,
    required: true
  },
  endTime: {
    type: Date,
    required: true
  },
  notif: {
    type: mongoose.Schema.Types.Boolean,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  bought: {
    type: Boolean,
    default: false
  }
})

const model = mongoose.model('AuctionProducts', schema)

module.exports = model