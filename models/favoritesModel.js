const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  userId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  userUsername: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  modelId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  modelUsername: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  userIp: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
})

const model = mongoose.model('Favorite', schema)

module.exports = model