// Dependencies
require('dotenv').config()
const redirectToHttps = require('./redirectToHttps')
const express = require('express')
const parseurl = require('parseurl')
const bodyParser = require('body-parser')
const path = require('path')
const expressValidator = require('express-validator')
const mongoose = require('mongoose')
const url = 'mongodb://localhost:27017/intimy'
const fileUpload = require('express-fileupload')
const bcrypt = require('bcrypt')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const jwt = require('jsonwebtoken')
const emailValidator = require('./my_modules/email_validator/emailValidator')
const app = express()
const fs = require('fs')
const mkdirp = require('mkdirp-promise')
const http = require('http')
const https = require('https')
const axios = require('axios')
const socketIO = require('socket.io')
const mailjet = require('node-mailjet')
    .connect('d10d558fc79a7cccfd9d59c2cc6aa813', 'd7503d373a22d0519be546affde56cd9')
const sharp = require("sharp")
const { forEach } = require('p-iteration')
const pid = process.pid
const stripe = require("stripe")("sk_live_bfoh7h5024JRSNaVxnN96MON00suiooMDe")
const redisAdapter = require('socket.io-redis')
const archiver = require('archiver')
var resumable = require('./resumable-node.js')((__dirname + "/public/uploads"))
const multipart = require('connect-multiparty')

const env = process.env.NODE_ENV || 'development'

app.use(express.static(__dirname + '/public'))
app.use(express.static(__dirname + '/statics'))
app.use(multipart())

const options = {
    key: fs.readFileSync('keys/private.key'),
    cert: fs.readFileSync('keys/__intimy_shop.pem'),
    ca: fs.readFileSync('keys/origin_ca_ecc_root.pem')
}

let server

if (env === "development") {
    server = http.createServer(app)
}

else {
    server = https.createServer(options, app)
    app.use((req, res, next) => {
        const host = req.header("host")
        if (host.match(/^www\..*/i)) {
            next()
        } else {
            res.redirect(301, "https://www." + host + req.url)
        }
    })
    redirectToHttps()
}

const io = socketIO(server)
io.adapter(redisAdapter({ host: 'localhost', port: 6379 }))

// Models
const buyersSchema = require('./models/buyersSchema.js')
const sellersSchema = require('./models/sellersSchema.js')
const produitsSchema = require('./models/produitsSchema.js')
const produitsPicturesModel = require('./models/produitsPicturesModel.js')
const produitsVideosModel = require('./models/produitsVideosModel.js')
const directSellProductsModel = require('./models/directSellProductsModel.js')
const notificationsSchema = require('./models/notificationsSchema.js')
const auctionProductsModel = require('./models/auctionProductsModel')
const emailListModel = require('./models/emailListModel')
const tokenspacksmodel = require('./models/tokenspacksmodel')
const shippingRatesModel = require('./models/shippingRatesModel')
const staffModel = require('./models/staffModel')
const withdrawRequestModel = require('./models/withdrawRequestModel')
const favoritesModel = require('./models/favoritesModel')
const itemsCommentsModel = require('./models/itemsCommentsModel')
const purchasesModel = require('./models/purchasesModel')

app.use(function (req, res, next) {
    let origin = req.headers.origin
    let originAllowed = ["http://localhost:3000", "http://localhost:8082", "https://intimy.shop", "https://www.intimy.shop", "https://transcoding.neoporn.net", "https://www.transcoding.neoporn.net"]
    if (originAllowed.indexOf(origin) != -1) {
        res.setHeader("Access-Control-Allow-Origin", origin)
    }
    res.header("Access-Control-Allow-Credentials", true)
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization')
    res.header("Access-Control-Max-Age", "1728000")
    if (req.method == "OPTIONS") {
        res.sendStatus(200)
        res.end()
    }
    else {
        next()
    }
})

app.use(bodyParser.urlencoded({ limit: "5000mb", extended: false }))
app.use(bodyParser.json({ limit: "5000mb" }))
app.use(require("body-parser").text())
app.use(cookieParser())

var nsp = io.of(`/notifications`)
var nspLiveStream = io.of(`/liveStream`)
var nspLiveDirectSell = io.of(`/LiveDirectSell`)
var nspPostProduit = io.of(`/postProduit`)
var nspStripeNewAccount = io.of(`/stripeNewAccount`)

nsp.on('connection', socket => {
    socket.join(`notifications/${socket.handshake.query.userId}`)
})

nspLiveStream.on('connection', socket => {
    socket.on('liveStream', data => {
        nspLiveStream.emit('connectLiveStream', data)
    })
})

nspLiveDirectSell.on('connection', socket => {
    socket.join(`LiveDirectSell/${socket.handshake.query.roomId}`)
})

app.post("/api/charge", async (req, res) => {
    try {
        let { status } = await stripe.charges.create({
            amount: 2000,
            currency: "usd",
            description: "An example charge",
            source: req.body
        });

        res.json({ status });
    } catch (err) {
        res.status(500).end();
    }
});

app.post('/api/connectstripe', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        sellersSchema.findOne({ _id: decoded.userId })
            .then(seller => {
                if (seller.stripeUserId) res.send("Code already exist")
                else {
                    axios.post(`https://connect.stripe.com/oauth/token`, { client_secret: "sk_test_k4A0QS2kPHYAC490dsQExotg", code: req.body.code, grant_type: "authorization_code" })
                        .then((response) => {
                            if (response.data.stripe_user_id) {
                                seller.stripeUserId = response.data.stripe_user_id
                                seller.save()
                                res.send("Ok")
                                nspStripeNewAccount.emit('Stripe new account')
                                stripe.accounts.update(response.data.stripe_user_id, {
                                    payout_schedule: { interval: "manual" }
                                })
                            }
                        })
                        .catch(err => console.log(err))
                }
            })
    })
})

app.post('/api/notifyMe', function (req, res) {
    if (req.body.id && req.body.emailAddress) {
        if (req.body.id != '1' && req.body.id != '2') {
            res.send('You are not allowed to do that!')
        }
        else {
            emailListModel.findOne({ emailAddress: req.body.emailAddress })
                .then(data => {
                    if (data) {
                        res.send('Already exist')
                    }
                    else if (!data) {
                        let emailAddress = new emailListModel(req.body)
                        emailAddress.save()
                        res.send('Ok')
                    }
                })
        }
    }
    else {
        res.send('A field is missing')
    }
})

app.get('/api/members', function (req, res) {
    sellersSchema.find({}).then(eachOne => {
        res.json(eachOne)
    })
})

app.get('/api/getprofildata', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (req.query.userStatus == 'buyer') {
            buyersSchema.findOne({ '_id': decoded.userId }).then(eachOne => {
                res.json(eachOne)
            })
        }
        if (req.query.userStatus == 'seller') {
            sellersSchema.findOne({ '_id': decoded.userId }).then(eachOne => {
                res.json(eachOne)
            })
        }
    })
})

app.get('/api/isverifieduser', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        sellersSchema.findOne({ '_id': decoded.userId })
            .select('verifiedUser idVerif stripeUserId')
            .then(user => {
                if (user.verifiedUser && user.stripeUserId) {
                    res.json({ stripeUserId: user.stripeUserId, message: 'User verified and ready' })
                }
                else if (user.verifiedUser) {
                    res.json({ message: 'User verified and ready' })
                }
                else if (!user.verifiedUser && user.idVerif) {
                    res.json({ message: 'Id verification pending' })
                }
                else if (!user.verifiedUser && !user.idVerif) {
                    res.json({ message: 'User not verified' })
                }
            })
    })
})

app.get('/api/getUsername', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (decoded.userStatus == 'buyer') {
            buyersSchema.findOne({ '_id': decoded.userId })
                .select('username profilPicture verifiedUser stripeUserId')
                .then(buyer => {
                    res.json({ user: buyer })
                })
        }
        if (decoded.userStatus == 'seller') {
            sellersSchema.findOne({ '_id': decoded.userId })
                .select('username profilPicture verifiedUser stripeUserId')
                .then(seller => {
                    if (seller.stripeUserId) {
                        stripe.balance.retrieve({
                            stripe_account: seller.stripeUserId
                        }, function (err, balance) {
                            res.json({ user: seller, balance: balance })
                        })
                    }
                    else {
                        res.json({ user: seller })
                    }
                })
        }
    })
})

app.get('/api/getbalance', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        sellersSchema.findOne({ _id: decoded.userId })
            .select("stripeUserId")
            .then(seller => {
                if (seller.stripeUserId) {
                    stripe.balance.retrieve({
                        stripe_account: seller.stripeUserId
                    }, function (err, balance) {
                        if (err) console.log(err)
                        res.json({ balance: balance })
                    })
                }
                else {
                    res.send("No Stripe account")
                }
            })
    })
})

app.get('/api/stripedashboard', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        sellersSchema.findOne({ _id: decoded.userId })
            .select("stripeUserId")
            .then(seller => {
                stripe.accounts.createLoginLink(
                    seller.stripeUserId,
                    function (err, link) {
                        if (err) console.log(err)
                        res.json({ message: "Ok", link: link })
                    }
                )
            })
    })
})

app.post('/api/withdrawalrequest', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (decoded.userStatus == 'seller') {
            if (req.body.amount >= 500) {
                sellersSchema.findOne({ _id: decoded.userId })
                    .then(async seller => {
                        const withdrawRequest = await withdrawRequestModel.findOne({ userId: decoded.userId })
                        if (withdrawRequest) {
                            res.send("Request already pending")
                        }
                        else {
                            if (seller.tokens >= req.body.amount) {
                                let newWithdrawRequestData = {}
                                newWithdrawRequestData.userId = await decoded.userId
                                newWithdrawRequestData.email = await seller.email
                                newWithdrawRequestData.amount = await req.body.amount
                                newWithdrawRequestData.userIp = await req.connection.remoteAddress
                                newWithdrawRequestData.paypalEmail = await req.body.paypalEmail
                                let newWithdrawRequest = await new withdrawRequestModel(newWithdrawRequestData)
                                newWithdrawRequest.save()
                                sellersSchema.updateOne({ _id: decoded.userId }, { $inc: { tokens: -req.body.amount } }).exec()
                                res.send("Ok")
                                nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', req.body.amount)
                            }
                            else {
                                res.send("Not enough tokens")
                            }
                        }
                    })
            }
            else {
                res.send("Amount too small")
            }
        }
    })
})

app.get('/api/getsellerdata', function (req, res) {
    if (mongoose.Types.ObjectId.isValid(req.query.id)) {
        sellersSchema.findOne({ '_id': req.query.id }).then(eachOne => {
            if (eachOne) {
                res.json(eachOne)
            }
            else {
                res.send('Redirect')
            }
        })
    }
    else {
        res.send('Redirect')
    }
})

app.get('/api/getitemstosell', async (req, res) => {
    try {
        const produits = await produitsSchema.find({ 'sellerId': req.query.sellerId, 'bought': false, 'directSell': req.query.directSell })
        const produitsPictures = await produitsPicturesModel.find({ 'sellerId': req.query.sellerId, 'deleted': { $ne: true } }).select('-pictures')
        const produitsVideos = await produitsVideosModel.find({ 'sellerId': req.query.sellerId, 'deleted': { $ne: true } }).select('-videos')
        res.json({ produits, produitsPictures, produitsVideos })
    } catch (error) {
        console.log(error)
    }
})

app.get('/api/getmyitemssold', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        try {
            const produits = await produitsSchema.find({ 'sellerId': decoded.userId, 'bought': true }).sort({ "boughtOn": 1 })
            const produitsPictures = await produitsPicturesModel.find({ 'sellerId': decoded.userId, 'boughtBy': { $exists: true, $not: { $size: 0 } } }).sort({ lastSale: 1 })
            const produitsVideos = await produitsVideosModel.find({ 'sellerId': decoded.userId, 'boughtBy': { $exists: true, $not: { $size: 0 } } }).sort({ lastSale: 1 })
            const directProduits = await directSellProductsModel.find({ 'sellerId': decoded.userId, 'bought': true })
            res.json({ produits, directProduits, produitsPictures, produitsVideos })
        } catch (error) {
            console.log(error)
        }

    })
})

app.get('/api/getmyitemsbought', async (req, res) => {
    try {
        jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
            const produits = await produitsSchema.find({ 'buyerId': decoded.userId, 'bought': true })
            const directSell = await directSellProductsModel.find({ 'buyerId': decoded.userId, 'bought': true })
            const produitsPictures = await produitsPicturesModel.find({ 'boughtBy': { $in: decoded.userId, $exists: true, $not: { $size: 0 } } }).sort({ lastSale: 1 })
            const produitsVideos = await produitsVideosModel.find({ 'boughtBy': { $in: decoded.userId, $exists: true, $not: { $size: 0 } } }).sort({ lastSale: 1 })
            res.json({ produits, directSell, produitsPictures, produitsVideos })
        })
    } catch (error) {
        console.log(error)
    }
})

app.get('/api/getmyfavorites', async (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        try {
            const favorites = await favoritesModel.find({ 'userId': decoded.userId })
            if (favorites) {
                let modelIds = []
                await favorites.forEach(async favorite => {
                    modelIds.push(favorite.modelId)
                })
                const cardsMyFavorites = await sellersSchema.find({ _id: { $in: modelIds } })
                    .select("username profilPicture live aboutMe.bio")
                res.send(cardsMyFavorites)
            }
            else res.send("No favorites")
        } catch (error) {
            console.log(error)
        }
    })
})

app.get('/api/getfavorites', async (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        const favorites = await favoritesModel.find({ 'userId': decoded.userId, 'modelId': req.query.modelId })
        if (favorites.length > 0) {
            res.json({ favorite: true })
        }
        else res.json({ favorite: false })
    })
})

app.post('/api/addfavorites', async (req, res) => {
    if (req.body.modelId) {
        jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async function (err, decoded) {
            let member
            if (decoded.userStatus === "seller") {
                member = await sellersSchema.findOne({ _id: decoded.userId }).select("username")
            }
            else {
                member = await buyersSchema.findOne({ _id: decoded.userId }).select("username")
            }
            favoritesModel.findOne({ userId: decoded.userId, modelId: req.body.modelId })
                .then(data => {
                    if (data) {
                        data.deleteOne()
                            .then(() => {
                                res.send('Favorite removed')
                            })
                    }
                    else {
                        let favorite = {}
                        favorite.modelId = req.body.modelId
                        favorite.modelUsername = req.body.modelUsername
                        favorite.userId = decoded.userId
                        favorite.userUsername = member.username
                        favorite.userIp = req.connection.remoteAddress
                        const newFavorite = new favoritesModel(favorite)
                        newFavorite.save()
                            .then(() => {
                                res.send('Favorite added')
                            })
                    }
                })
        })
    }
    else {
        res.send('Allo?')
    }
})

app.get('/api/getitemstodirectsell', async (req, res) => {
    const directSellProducts = await directSellProductsModel.findOne({ 'sellerId': req.query.sellerId, 'bought': false })
    res.json(directSellProducts)
})

app.get('/api/getitemtosell', async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.query.itemId)) {
        try {
            const produits = await produitsSchema.findOne({ "_id": req.query.itemId, 'bought': false })
            const produitsPictures = await produitsPicturesModel.findOne({ "_id": req.query.itemId, 'deleted': { $ne: true } })
            const produitsVideos = await produitsVideosModel.findOne({ "_id": req.query.itemId, 'deleted': { $ne: true } })
            if (!produits && !produitsPictures && !produitsVideos) {
                res.send('Nothing here')
            }
            else {
                res.json({ produits, produitsPictures, produitsVideos })
            }
        } catch (error) {
            console.log(error)
        }
    }
    else {
        res.send('Nothing here')
    }
})

app.get('/api/getitemlink', async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.query.itemId)) {
        try {
            const produitsPictures = await produitsPicturesModel.findOne({ "_id": req.query.itemId }).select('pictures')
            if (!produitsPictures) {
                res.send('Nothing here')
            }
            else {
                res.send(produitsPictures)
            }
        } catch (error) {
            console.log(error)
        }
    }
    else {
        res.send('Nothing here')
    }
})

app.get('/api/getitemlinkvideo', async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.query.itemId)) {
        try {
            const produitsVideos = await produitsVideosModel.findOne({ "_id": req.query.itemId }).select('videos')
            if (!produitsVideos) {
                res.send('Nothing here')
            }
            else {
                res.send(produitsVideos)
            }
        } catch (error) {
            console.log(error)
        }
    }
    else {
        res.send('Nothing here')
    }
})

app.get('/api/getitemtosellpictures', function (req, res) {
    if (mongoose.Types.ObjectId.isValid(req.query.itemId)) {
        produitsPicturesModel.findOne({ "_id": req.query.itemId, 'deleted': { $ne: true } })
            .select('-pictures')
            .exec((err, item) => {
                if (err) {
                    res.send(err)
                }
                else if (!item || item.length == 0) {
                    res.send('Nothing here')
                }
                else {
                    res.send(item)
                }
            })
    }
    else {
        res.send('Nothing here')
    }
})

app.get('/api/getitemtosellvideos', function (req, res) {
    if (mongoose.Types.ObjectId.isValid(req.query.itemId)) {
        produitsVideosModel.findOne({ "_id": req.query.itemId, 'deleted': { $ne: true } })
            .select('-videos')
            .exec((err, item) => {
                if (err) {
                    res.send(err)
                }
                else if (!item || item.length == 0) {
                    res.send('Nothing here')
                }
                else {
                    res.send(item)
                }
            })
    }
    else {
        res.send('Nothing here')
    }
})

app.get('/api/getmyitemstosell', async (req, res) => {
    try {
        const produits = await produitsSchema.find({ 'sellerId': req.query.userId, 'bought': false })
        const produitsPictures = await produitsPicturesModel.find({ 'sellerId': req.query.userId, 'deleted': { $ne: true } }).select('-pictures')
        const produitsVideos = await produitsVideosModel.find({ 'sellerId': req.query.userId, 'deleted': { $ne: true } }).select('-videos')
        res.json({ produits, produitsPictures, produitsVideos })
    } catch (error) {
        console.log(error)
    }
})

app.get('/api/getHomeCards', async (req, res) => {
    try {
        let filter
        if (req.query.idAlreadyGot) filter = await { emailVerified: true, verifiedUser: true }
        else filter = await { emailVerified: true, verifiedUser: true }
        var sellers = await sellersSchema.find(filter).sort({ lastActivity: -1 }).skip(parseInt(req.query.skipSize)).limit(21)
        var totalLength = await sellersSchema.countDocuments({ emailVerified: true, verifiedUser: true })
        res.json({ sellers, totalLength })
    } catch (err) {
        console.log(err)
    }
})

app.get('/api/searchForm', (req, res) => {
    const parsedQuery = JSON.parse(req.query.dataSearchForm)
    let newQuery = { emailVerified: true }
    if (parsedQuery.origins) newQuery.origins = parsedQuery.origins
    if (parsedQuery.username) newQuery.username = parsedQuery.username
    if (parsedQuery.age) newQuery = { "aboutMe.age": parsedQuery.age, emailVerified: true }
    sellersSchema.find(newQuery)
        .then(data => {
            if (!data.length) {
                res.send("No data")
            }
            else {
                res.send(data)
            }
        })
})

app.get('/api/getbroadcasters', function (req, res) {
    sellersSchema.find({ live: true }, (err, sellers) => {
        if (err) {
            res.send(err)
        }
        else {
            res.json(sellers)
        }
    })
})

app.get('/api/getIdVerificationRequests', (req, res) => {
    sellersSchema.find({ verifiedUser: false, idVerif: { $exists: true } })
        .select('_id idVerif')
        .then(users => {
            if (users) {
                res.json(users)
            }
        })
})

app.get('/api/getwithdrawrequests', (req, res) => {
    withdrawRequestModel.find({ verified: false })
        .then(users => {
            if (users) {
                res.json(users)
            }
        })
})

app.put('/api/idRequestValidation', (req, res) => {
    if (req.body.validation.selectedOption === 'yes') {
        sellersSchema.findOne({ _id: req.body.userId })
            .then(member => {
                sellersSchema.updateOne({ _id: req.body.userId }, { $set: { verifiedUser: true } })
                    .then(() => {
                        res.send("Ok")
                        const notification = {
                            notifType: 'verifId',
                            userId: req.body.userId,
                            verifId: {
                                stateVerification: true
                            }
                        }
                        let newNotification = new notificationsSchema(notification)
                        newNotification.save()
                        nsp.to(`notifications/${req.body.userId}`).emit('New notification')
                        const request = mailjet
                            .post("send", { 'version': 'v3.1' })
                            .request({
                                "Messages": [
                                    {
                                        "From": {
                                            "Email": "intimy.shop@gmail.com",
                                            "Name": "Intimy ID verification"
                                        },
                                        "To": [
                                            {
                                                "Email": `${member.email}`
                                            }
                                        ],
                                        "Subject": "ID verification verified",
                                        "HTMLPart": `Hello ${member.username}, Congratulations! <br /> Your ID has been verified!`,
                                        "CustomID": "IdVerification"
                                    }
                                ]
                            })
                        request
                            .then((result) => {
                                console.log(result.body)
                            })
                            .catch((err) => {
                                console.log(err.statusCode)
                            })
                    })
            })
    }
    else if (req.body.validation.selectedOption === 'no') {
        sellersSchema.findOne({ _id: req.body.userId })
            .then(member => {
                sellersSchema.updateOne({ _id: req.body.userId }, { $unset: { idVerif: 1 } })
                    .then(() => {
                        res.send("No")
                        const notification = {
                            notifType: 'verifId',
                            userId: req.body.userId,
                            verifId: {
                                stateVerification: false,
                                reason: req.body.validation.reason
                            }
                        }
                        let newNotification = new notificationsSchema(notification)
                        newNotification.save()
                        const request = mailjet
                            .post("send", { 'version': 'v3.1' })
                            .request({
                                "Messages": [
                                    {
                                        "From": {
                                            "Email": "intimy.shop@gmail.com",
                                            "Name": "Intimy ID verification"
                                        },
                                        "To": [
                                            {
                                                "Email": `${member.email}`
                                            }
                                        ],
                                        "Subject": "ID verification rejected",
                                        "HTMLPart": `Hello ${member.username}, <br /> Oooh! We are sorry to tell you that your ID was rejected because: ${req.body.validation.reason}<br />`,
                                        "CustomID": "IdVerification"
                                    }
                                ]
                            })
                        request
                            .then((result) => {
                                console.log(result.body)
                            })
                            .catch((err) => {
                                console.log(err.statusCode)
                            })
                        nsp.to(`notifications/${req.body.userId}`).emit('New notification')
                    })
            })
    }
})

app.put('/api/withdrawRequestValidation', async (req, res) => {
    if (req.body.validation.selectedOption === 'yes') {
        const seller = await sellersSchema.findOne({ _id: req.body.userId })
        withdrawRequestModel.updateOne({ _id: req.body.userId }, { $set: { validate: true } })
            .then(() => {
                res.send("Ok")
                const notification = {
                    notifType: 'withdrawRequest',
                    userId: req.body.userId,
                    withdrawRequest: {
                        validated: true
                    }
                }
                let newNotification = new notificationsSchema(notification)
                newNotification.save()
                const request = mailjet
                    .post("send", { 'version': 'v3.1' })
                    .request({
                        "Messages": [
                            {
                                "From": {
                                    "Email": "intimy.shop@gmail.com",
                                    "Name": "Intimy withdrawal request"
                                },
                                "To": [
                                    {
                                        "Email": `${seller.email}`
                                    }
                                ],
                                "Subject": "Withdrawal request approved",
                                "HTMLPart": `Hello ${seller.username}, <br /> We are happy to tell you that your withdrawal request has been approved`,
                                "CustomID": "WithdrawalRequest"
                            }
                        ]
                    })
                request
                    .then((result) => {
                        console.log(result.body)
                    })
                    .catch((err) => {
                        console.log(err.statusCode)
                    })
                nsp.to(`notifications/${req.body.userId}`).emit('New notification')
            })
    }
    else if (req.body.validation.selectedOption === 'no') {
        const seller = await sellersSchema.findOne({ _id: req.body.userId })
        const withdrawrequest = await withdrawRequestModel.findOne({ userId: req.body.userId })
        withdrawRequestModel.updateOne({ userId: seller._id }, { verified: true, validated: false }).exec()
        sellersSchema.updateOne({ _id: seller._id }, { $inc: { tokens: withdrawrequest.amount } })
            .then(() => {
                res.send("No")
                const notification = {
                    notifType: 'withdrawRequest',
                    userId: req.body.userId,
                    withdrawRequest: {
                        validated: false,
                        reason: req.body.validation.reason
                    }
                }
                let newNotification = new notificationsSchema(notification)
                newNotification.save()
                const request = mailjet
                    .post("send", { 'version': 'v3.1' })
                    .request({
                        "Messages": [
                            {
                                "From": {
                                    "Email": "intimy.shop@gmail.com",
                                    "Name": "Intimy withdrawal request"
                                },
                                "To": [
                                    {
                                        "Email": `${seller.email}`
                                    }
                                ],
                                "Subject": "Withdrawal request rejected",
                                "HTMLPart": `Hello ${seller.username}, <br /> We are sorry to tell you that your withdrawal request has bees rejected because: ${req.body.validation.reason}<br />`,
                                "CustomID": "WithdrawalRequest"
                            }
                        ]
                    })
                request
                    .then((result) => {
                        console.log(result.body)
                    })
                    .catch((err) => {
                        console.log(err.statusCode)
                    })
                nsp.to(`notifications/${seller._id}`).emit('New notification')
                nsp.to(`notifications/${seller._id}`).emit('Tokens substract', withdrawrequest.amount)

            })
    }
})

app.get('/api/getnotifsnumber', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        const notifsNumber = await notificationsSchema.countDocuments({ userId: decoded.userId, seen: false })
        res.json({ notifsNumber })
    })
})

app.put('/api/notificationViewed', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        notificationsSchema.updateOne({ _id: req.body.notifId }, { $set: { seen: true } })
            .then(data => {
                nsp.to(`notifications/${decoded.userId}`).emit('New notification')
                res.send("Notif viewed")
            })
            .catch(err => { throw err })
    })
})

app.post('/api/idverif', (req, res) => {
    if (req.body.idVerif.idPhoto && req.body.idVerif.idFace) {
        sellersSchema.countDocuments({ _id: req.body.userId, idVerif: { $exists: true } })
            .then(idVerif => {
                if (!idVerif) {
                    if (req.body.idVerif.idPhoto) {
                        let ext = req.body.idVerif.idPhoto.split(';')[0].match(/jpeg|png|gif/)
                        if (ext) {
                            let data = req.body.idVerif.idPhoto.replace(/^data:image\/\w+;base64,/, "")
                            let buffer = new Buffer.from(data, 'base64')
                            let timestamp = new Date().getTime().toString()
                            let randomInt = Math.floor((Math.random() * 100) + 1)
                            let path = "sellers/" + req.body.userId + "/idVerif/" + timestamp + randomInt + "." + ext
                            mkdirp("public/sellers/" + req.body.userId + "/idVerif/", err => {
                                if (err) throw err
                                fs.writeFile('public/' + path, buffer, err => {
                                    if (err) throw err
                                })
                            })
                            req.body.idVerif.idPhoto = `${process.env.ORIGIN}/${path}`
                        }
                        else {
                            errorExtProfilPicture = true
                        }
                    }
                    if (req.body.idVerif.idFace) {
                        let ext = req.body.idVerif.idFace.split(';')[0].match(/jpeg|png|gif/)
                        if (ext) {
                            let data = req.body.idVerif.idFace.replace(/^data:image\/\w+;base64,/, "")
                            let buffer = new Buffer.from(data, 'base64')
                            let timestamp = new Date().getTime().toString()
                            let randomInt = Math.floor((Math.random() * 100) + 1)
                            let path = "sellers/" + req.body.userId + "/idVerif/" + timestamp + randomInt + "." + ext
                            mkdirp("public/sellers/" + req.body.userId + "/idVerif/", err => {
                                if (err) throw err
                                fs.writeFile('public/' + path, buffer, err => {
                                    if (err) throw err
                                })
                            })
                            req.body.idVerif.idFace = `${process.env.ORIGIN}/${path}`
                        }
                        else {
                            errorExtProfilPicture = true
                        }
                    }
                    sellersSchema.updateOne({ _id: req.body.userId }, { $set: { idVerif: req.body.idVerif } })
                        .then(() => {
                            res.send("ID sent")
                        })
                    const request = mailjet
                        .post("send", { 'version': 'v3.1' })
                        .request({
                            "Messages": [
                                {
                                    "From": {
                                        "Email": "intimy.shop@gmail.com",
                                        "Name": "Intimy ID verification"
                                    },
                                    "To": [
                                        {
                                            "Email": `intimy.shop@gmail.com`
                                        }
                                    ],
                                    "Subject": "New ID verification request",
                                    "HTMLPart": `New ID verification request <a href="https://www.intimy.shop/backoffice">Intimy ID verification</a>`,
                                    "CustomID": "IdVerification"
                                }
                            ]
                        })
                    request
                        .then((result) => {
                            console.log(result.body)
                        })
                        .catch((err) => {
                            console.log(err.statusCode)
                        })
                }
                else {
                    res.send("ID already sent")
                }
            })
    }
    else {
        res.send("A document is missing")
    }
})

app.post('/api/postproduitpictures', function (req, res, next) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        let newItem = {}
        newItem.name = req.body.name.trim()
        newItem.description = req.body.description
        newItem.sellerId = decoded.userId
        newItem.img = `${process.env.ORIGIN}/images/picturesSet.svg`
        newItem.numberPictures = req.body.pictures.length
        if (req.body.pictures) {
            if (req.body.pictures.length > 100) {
                res.send('Pictures limit reached')
                return next('Pictures limit reached')
            }
            else {
                let str = req.body.name
                let i = 0, strLength = str.length
                for (i; i < strLength; i++) {
                    str = str.replace(" ", "_")
                }
                let randomNumber = Math.floor((Math.random() * 100) + 1) + new Date().getTime().toString()
                await mkdirp("public/itemsPicturesPictures/" + decoded.userId + "/")
                var output = fs.createWriteStream("public/itemsPicturesPictures/" + decoded.userId + "/" + str + "_" + randomNumber + ".zip")
                var archive = archiver('zip', {
                    zlib: { level: 9 } // Sets the compression level.
                })

                // listen for all archive data to be written
                // 'close' event is fired only when a file descriptor is involved
                output.on('close', function () {
                    console.log(archive.pointer() + ' total bytes')
                    console.log('archiver has been finalized and the output file descriptor has closed.')
                })

                // This event is fired when the data source is drained no matter what was the data source.
                // It is not part of this library but rather from the NodeJS Stream API.
                // @see: https://nodejs.org/api/stream.html#stream_event_end
                output.on('end', function () {
                    console.log('Data has been drained')
                })

                // good practice to catch warnings (ie stat failures and other non-blocking errors)
                archive.on('warning', function (err) {
                    if (err.code === 'ENOENT') {
                        console.log(err)
                    } else {
                        // throw error
                        throw err
                    }
                })

                // good practice to catch this error explicitly
                archive.on('error', function (err) {
                    throw err
                })

                // pipe archive data to the file
                archive.pipe(output)
                newItem.pictures = `${process.env.ORIGIN}/itemsPicturesPictures/${decoded.userId}/${str}_${randomNumber}.zip`
                await forEach(req.body.pictures, async pictures => {
                    let ext = pictures.split(';')[0].match(/jpeg|png/)
                    if (ext) {
                        let data = pictures.replace(/^data:image\/\w+;base64,/, "")
                        let timestamp = new Date().getTime().toString()
                        let randomInt = Math.floor((Math.random() * 100) + 1)
                        var buffer3 = Buffer.from(data, 'base64')
                        archive.append(buffer3, { name: timestamp + randomInt + "." + ext })
                    }
                    else {
                        errorExtProfilPicture = true
                    }
                })
                archive.finalize()
            }
        }
        newItem.price = Number(req.body.price)

        await sellersSchema.updateOne({ _id: decoded.userId }, { $set: { lastActivity: Date.now() } }, { upsert: true })

        let produit = new produitsPicturesModel(newItem)
        produit.save()
            .then(() => {
                nspPostProduit.emit('Produit posted')
                res.send("Your item is online!")
            })
    })
})

app.post('/api/postproduitvideos', function (req, res, next) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        let newItem = {}
        newItem.name = req.body.formDataVideos.name.trim()
        newItem.description = req.body.formDataVideos.description
        newItem.sellerId = decoded.userId
        newItem.img = `${process.env.ORIGIN}/images/videosSet.svg`
        newItem.numberVideos = req.body.formDataVideos.videosLength
        newItem.price = Number(req.body.formDataVideos.price)
        await sellersSchema.updateOne({ _id: decoded.userId }, { $set: { lastActivity: Date.now() } }, { upsert: true })
        let produit = new produitsVideosModel(newItem)
        produit.save()
            .then(() => {
                nspPostProduit.emit('Produit posted')
                res.send("Your item is online!")
            })
    })
})

app.post('/api/postproduit', function (req, res, next) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        const seller = await sellersSchema.findOne({ _id: decoded.userId }).select('aboutMe.country')
        let newItem = {}
        newItem.name = req.body.name.trim()
        newItem.description = req.body.description
        newItem.sellerId = decoded.userId
        newItem.directSell = req.body.directSell
        newItem.shippingOption = req.body.shippingOption
        newItem.country = seller.aboutMe.country
        newItem.pictures = []
        let ext = req.body.img.split(';')[0].match(/jpeg|png|gif/)[0]
        let data = req.body.img.replace(/^data:image\/\w+;base64,/, "")
        let buffer = new Buffer.from(data, 'base64')
        let timestamp = new Date().getTime().toString()
        let path = "itemsPictures/" + decoded.userId + "/imgs/" + timestamp + "." + ext
        await mkdirp("public/itemsPictures/" + decoded.userId + "/imgs/")
        await sharp(buffer)
            .rotate()
            .resize(300, 306)
            .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
            .jpeg({ quality: 10 })
            .png({ compressionLevel: 9 })
            .toFile('public/' + path)
        newItem.img = `${process.env.ORIGIN}/${path}`
        if (req.body.pictures) {
            if (req.body.pictures.length >= 8) {
                res.send('Pictures limit reached')
                return next('Pictures limit reached')
            }
            else {
                await forEach(req.body.pictures, async pictures => {
                    let ext = pictures.split(';')[0].match(/jpeg|png/)
                    if (ext) {
                        let data = pictures.replace(/^data:image\/\w+;base64,/, "")
                        let buffer = new Buffer.from(data, 'base64')
                        let timestamp = new Date().getTime().toString()
                        let randomInt = Math.floor((Math.random() * 100) + 1)
                        let path = "itemsPictures/" + decoded.userId + "/imgs/" + timestamp + randomInt + "." + ext
                        let pathMin = "itemsPictures/" + decoded.userId + "/imgs/min/" + timestamp + randomInt + "-min." + ext
                        await mkdirp("public/itemsPictures/" + decoded.userId + "/imgs/min/")
                        const sharpPictures = await sharp(buffer)
                            .withMetadata()
                            .rotate()
                            .resize(800, 1000, { fit: "inside" })
                            .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                            .jpeg({ quality: 10 })
                            .png({ compressionLevel: 9 })
                            .toFile('public/' + path)
                        await sharp(buffer)
                            .rotate()
                            .resize(300, 306)
                            .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                            .jpeg({ quality: 10 })
                            .png({ compressionLevel: 9 })
                            .toFile('public/' + pathMin)
                        newItem.pictures.push({ src: `${process.env.ORIGIN}/${path}`, thumbnail: `${process.env.ORIGIN}/${pathMin}`, w: sharpPictures.width, h: sharpPictures.height })
                    }
                    else {
                        errorExtProfilPicture = true
                    }
                })
            }
        }
        newItem.price = Number(req.body.price)

        await sellersSchema.updateOne({ _id: decoded.userId }, { $set: { lastActivity: Date.now() } }, { upsert: true })

        let produit = new produitsSchema(newItem)
        produit.save()
            .then(() => {
                nspPostProduit.emit('Produit posted')
                res.send("Your item is online!")
            })
    })
})

app.post('/api/postdirectsellproduit', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        directSellProductsModel.find({ sellerId: decoded.userId, bought: false })
            .then(async data => {
                if (data.length < 1) {
                    const seller = await sellersSchema.findOne({ _id: decoded.userId }).select('aboutMe.country')
                    let newItem = {}
                    newItem.name = req.body.name.trim()
                    newItem.sellerId = decoded.userId
                    newItem.notif = req.body.notif
                    newItem.shippingOption = req.body.shippingOption
                    newItem.country = seller.aboutMe.country
                    let ext = req.body.img.split(';')[0].match(/jpeg|png|gif/)[0]
                    let data = req.body.img.replace(/^data:image\/\w+;base64,/, "")
                    let buffer = new Buffer.from(data, 'base64')
                    let timestamp = new Date().getTime().toString()
                    let path = "itemsPictures/" + decoded.userId + "/imgs/" + timestamp + "." + ext
                    mkdirp("public/itemsPictures/" + decoded.userId + "/imgs/", err => {
                        if (err) throw err
                        fs.writeFile('public/' + path, buffer, err => {
                            if (err) throw err
                        })
                    })
                    newItem.img = `${process.env.ORIGIN}/${path}`

                    newItem.price = Number(req.body.price)

                    let produit = new directSellProductsModel(newItem)
                    produit.save()
                        .then(() => {
                            nspLiveDirectSell.to(`LiveDirectSell/${decoded.userId}`).emit('Direct sell item added')
                            res.send("Your item is online!")
                        })
                }
                else {
                    res.send('Direct sell already in progress')
                }
            })
    })
})

app.post('/api/postauctionproduit', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        auctionProductsModel.find({ sellerId: decoded.userId, bought: false })
            .then(data => {
                if (data.length < 1) {
                    req.body.name = req.body.name.trim()
                    req.body.sellerId = decoded.userId
                    let produit = new auctionProductsModel(req.body)
                    produit.save()
                        .then(() => {
                            nspLiveDirectSell.to(`LiveDirectSell/${decoded.userId}`).emit('Auction sell item added')
                            res.send("Your item is online!")
                        })
                }
                else {
                    res.send('Auction sell already in progress')
                }
            })
    })
})

app.post('/api/addtomyshoppingbag', (req, res) => {
    if (req.body.connected) {
        jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
            if (decoded.userStatus == 'buyer') {
                buyersSchema.findById(decoded.userId)
                    .select('shoppingbag')
                    .exec()
                    .then(data => {
                        if (data.shoppingbag.indexOf(req.body.itemId) > -1) {
                            res.send('This item is already in your shopping bag')
                        }
                        else {
                            nsp.to(`notifications/${decoded.userId}`).emit('New notification shopping bag', 'Add')
                            data.shoppingbag.push(req.body.itemId)
                            data.save()
                                .then(updatedData => {
                                    res.send('Item added')
                                })
                                .catch(err => { throw err })
                        }
                    })
            }
            if (decoded.userStatus == 'seller') {
                sellersSchema.findById(decoded.userId)
                    .select('shoppingbag')
                    .exec()
                    .then(data => {
                        if (data.shoppingbag.indexOf(req.body.itemId) > -1) {
                            res.send('This item is already in your shopping bag')
                        }
                        else {
                            nsp.to(`notifications/${decoded.userId}`).emit('New notification shopping bag', 'Add')
                            data.shoppingbag.push(req.body.itemId)
                            data.save()
                                .then(updatedData => {
                                    res.send('Item added')
                                })
                                .catch(err => { throw err })
                        }
                    })
            }
        })
    }
    else {
        res.send("Not connected")
    }
})

app.get('/api/getitemsfrommyshoppingbag', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (decoded.userStatus == 'buyer') {
            buyersSchema.findById(decoded.userId)
                .select('shoppingbag')
                .exec()
                .then(data => {
                    if (err) throw err
                    produitsSchema.find({ '_id': { $in: data.shoppingbag }, 'bought': false }, (err, sellers) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send(sellers)
                    })
                })
                .catch(err => { throw err })
        }
        if (decoded.userStatus == 'seller') {
            sellersSchema.findById(decoded.userId)
                .select('shoppingbag')
                .exec()
                .then(data => {
                    if (err) throw err
                    produitsSchema.find({ '_id': { $in: data.shoppingbag } }, (err, sellers) => {
                        if (err) {
                            res.send(err)
                        }
                        res.send(sellers)
                    })
                })
                .catch(err => { throw err })
        }
    })
})

app.get('/api/getitemsfrommyshoppingbagnotconnected', (req, res) => {
    produitsSchema.find({ '_id': { $in: req.query.itemsId }, 'bought': false }, (err, produits) => {
        if (err) {
            res.send(err)
        }
        res.send(produits)
    })
})

app.put('/api/removefrommyshoppingbag', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (decoded.userStatus == 'buyer') {
            buyersSchema.findById(decoded.userId)
                .select('shoppingbag')
                .exec()
                .then(data => {
                    if (data.shoppingbag.indexOf(req.body.itemId) > -1) {
                        nsp.to(`notifications/${decoded.userId}`).emit('New notification shopping bag', 'Remove')
                        data.shoppingbag.remove(req.body.itemId)
                        data.save()
                        res.send('Item removed')
                    }
                    else {
                        res.send('No item to remove')
                    }
                }, err => {
                    throw err
                })
        }
        if (decoded.userStatus == 'seller') {
            sellersSchema.findById(decoded.userId)
                .select('shoppingbag')
                .exec()
                .then(data => {
                    if (data.shoppingbag.indexOf(req.body.itemId) > -1) {
                        nsp.to(`notifications/${decoded.userId}`).emit('New notification shopping bag', 'Remove')
                        data.shoppingbag.remove(req.body.itemId)
                        data.save()
                        res.send('Item removed')
                    }
                    else {
                        res.send('No item to remove')
                    }
                }, err => {
                    throw err
                })
        }
    })
})

app.post('/api/getshippingrates', async (req, res) => {
    try {
        var rates = 0
        await forEach(req.body.items, async item => {
            var sellerCountry
            switch (item.country) {
                case "France":
                    sellerCountry = "FR";
                    break;
                case "United States":
                    sellerCountry = "US";
                    break;
            }
            await sellersSchema.findOne({ _id: item.sellerId })
                .then(async seller => {
                    const datas = {
                        "shipment": {
                            "validate_address": "no_validation",
                            "ship_to": {
                                "name": "Intimyto",
                                "phone": "714-781-4564",
                                "company_name": "The Walt Disney Company",
                                "address_line1": req.body.myAddress.streetaddress,
                                "city_locality": 'New York City',
                                "state_province": req.body.myAddress.state,
                                "postal_code": req.body.myAddress.zipcode,
                                "country_code": req.body.myAddress.country
                            },
                            "ship_from": {
                                "name": "Intimyfrom",
                                "phone": "714-781-4565",
                                "company_name": "The Walt Disney Company2",
                                "address_line1": "834 Quincy st",
                                "city_locality": 'New York City',
                                "state_province": "NY",
                                "postal_code": "11221",
                                "country_code": 'US'
                            },
                            "customs": {
                                "contents": "merchandise",
                                "customs_items": [
                                    {
                                        "description": "lingerie",
                                        "quantity": 1,
                                        "value": 10.0,
                                        "country_of_origin": "US"
                                    }
                                ],
                                "non_delivery": "return_to_sender"
                            },
                            "packages": [
                                {
                                    "weight": {
                                        "value": 1.0,
                                        "unit": "ounce"
                                    }
                                }
                            ]
                        },
                        "rate_options": {
                            "carrier_ids": [
                                "se-282199"
                            ]
                        }
                    }
                    const headers = {
                        headers: {
                            "Content-type": "application/json",
                            "api-key": "X9K4DSDE+HvtXouVf79I5xjCrC/UwrLdYAXUt6tiT3o"
                        }
                    }
                    try {
                        const shippingRates = await axios.post('https://api.shipengine.com/v1/rates', datas, headers)
                        const amount = await shippingRates.data.rate_response.rates.find(data => data.package_type === "flat_rate_padded_envelope")
                        rates += amount.shipping_amount.amount
                    }
                    catch (error) {
                        console.log(error.response.data.errors)
                    }
                })
        })
        res.json(rates)
    } catch (error) {
        console.log(error)
    }
})

app.delete('/api/deletePicture', (req, res) => {
    if (req.query.userId === req.query.posterId) {
        sellersSchema.updateOne({ _id: req.query.userId }, { $pull: { pictures: { _id: req.query.id } } })
            .then(data => {
                res.send('Picture deleted')
            })
            .catch(err => { throw err })
    }
    else {
        res.json('You are not authorized')
    }
})

app.put('/api/deleteitem', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', (err, decoded) => {
        produitsSchema.deleteOne({ _id: req.body.itemId })
            .then(data => {
                res.send('Item deleted')
            })
            .catch(err => { throw err })
    })
})

app.put('/api/deleteitempictures', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', (err, decoded) => {
        produitsPicturesModel.updateOne({ _id: req.body.itemId }, { $set: { deleted: true } })
            .then(data => {
                res.send('Item deleted')
            })
            .catch(err => { throw err })
    })
})

app.put('/api/deleteitemvideos', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', (err, decoded) => {
        produitsVideosModel.updateOne({ _id: req.body.itemId }, { $set: { deleted: true } })
            .then(data => {
                res.send('Item deleted')
            })
            .catch(err => { throw err })
    })
})

app.put('/api/deletedirectorauctionsellitem', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', (err, decoded) => {
        let typeModel
        if (req.body.sellType === "Direct") {
            typeModel = directSellProductsModel
        }
        else {
            typeModel = auctionProductsModel
        }
        typeModel.deleteOne({ _id: req.body.itemId })
            .exec()
            .then(data => {
                nspLiveDirectSell.to(`LiveDirectSell/${decoded.userId}`).emit('Direct sell item removed')
                res.send('Item deleted')
            })
            .catch(err => { throw err })
    })
})

app.post('/api/signin', function (req, res) {
    if (!req.body.email || !req.body.password) {
        res.send('A required field is missing')
    }
    else {
        req.body.email = req.body.email.trim()
        req.body.password = req.body.password.trim()
        try {
            sellersSchema.findOne({ $or: [{ email: { $regex: "^" + req.body.email + "$", $options: "i" } }, { username: { $regex: "^" + req.body.email + "$", $options: "i" } }] })
                .select('password emailVerified shoppingbag')
                .then((member) => {
                    if (member) {
                        bcrypt.compare(req.body.password, member.password).then(result => {
                            if (result == false) {
                                res.send('Wrong password')
                            }
                            else {
                                if (member.emailVerified === true) {
                                    jwt.sign({ userId: member._id, userStatus: 'seller', role: member.role }, 'usersupersecret4.0', async (err, token) => {
                                        if (err) throw err
                                        member.lastConnection = Date.now()
                                        if (req.body.shoppingBag.length > 0) {
                                            let pushOrNot = false
                                            for (const item of req.body.shoppingBag) {
                                                if (!member.shoppingbag.includes(item)) {
                                                    pushOrNot = await true
                                                }
                                            }
                                            if (pushOrNot) member.shoppingbag.push(req.body.shoppingBag)
                                        }
                                        await member.save()
                                        res.cookie('authtoken', token).send('Ok')
                                    })
                                }
                                else {
                                    res.send("Confirm email address")
                                }
                            }
                        })
                    }
                    if (!member) {
                        buyersSchema.findOne({ $or: [{ email: { $regex: "^" + req.body.email + "$", $options: "i" } }, { username: { $regex: "^" + req.body.email + "$", $options: "i" } }] })
                            .select('password emailVerified shoppingbag')
                            .then((member) => {
                                if (member) {
                                    bcrypt.compare(req.body.password, member.password).then(result => {
                                        if (result == false) {
                                            res.send('Wrong password')
                                        }
                                        else {
                                            if (member.emailVerified === true) {
                                                jwt.sign({ userId: member._id, userStatus: 'buyer' }, 'usersupersecret4.0', async (err, token) => {
                                                    if (err) throw err
                                                    if (req.body.shoppingBag.length > 0) {
                                                        let pushOrNot = false
                                                        for (const item of req.body.shoppingBag) {
                                                            if (!member.shoppingbag.includes(item)) {
                                                                pushOrNot = await true
                                                            }
                                                        }
                                                        if (pushOrNot) member.shoppingbag.push(req.body.shoppingBag)
                                                    }
                                                    await member.save()
                                                    res.cookie('authtoken', token).send('Ok')
                                                })
                                            }
                                            else res.send("Confirm email address")
                                        }
                                    })
                                }
                                else {
                                    res.send('No account founded')
                                }
                            })
                    }
                })
        } catch (error) {
            throw error
        }
    }
})

app.put('/api/setlivestatus', (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.body.userId)) {
        jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', (err, decoded) => {
            sellersSchema.updateOne({ _id: req.body.userId }, { $set: { live: req.body.live } })
                .exec()
                .then(data => {
                    res.send('Live status changed')
                })
                .catch(err => { console.log(err) })
        })
    }
    else {
        res.send("Already")
    }
})

//Reset password request, send an email with the link
app.get('/api/forgotpassword', async (req, res) => {
    const seller = await sellersSchema.findOne({ email: req.query.email })
    const buyer = await buyersSchema.findOne({ email: req.query.email })
    let model
    if (!seller && !buyer) {
        res.send("No account found")
    }
    else if (seller) model = await seller
    else if (buyer) model = await buyer

    const secret = await model.password + "-" + model.date.getTime()
    jwt.sign({ userId: model._id }, secret, async (err, token) => {
        if (err) throw err
        const request = mailjet
            .post("send", { 'version': 'v3.1' })
            .request({
                "Messages": [
                    {
                        "From": {
                            "Email": "intimy.shop@gmail.com",
                            "Name": "Intimy"
                        },
                        "To": [
                            {
                                "Email": `${req.query.email}`
                            }
                        ],
                        "Subject": "Intimy reset password",
                        "HTMLPart": `You asked to reset your password <br /> Click the link below to reset your password <br /> <a href="${process.env.ORIGIN}/resetmypassword/${model._id}/${token}">Reset my password</a>`,
                        "CustomID": "PasswordReset"
                    }
                ]
            })
        request
            .then((result) => {
                console.log(result.body)
            })
            .catch((err) => {
                console.log(err.statusCode)
            })
        res.send('Ok')
    })
})

//Check if the token is valid to authorize or not the user to change his password
app.get('/api/authresetmypassword', async (req, res) => {
    const seller = await sellersSchema.findOne({ _id: req.query.id })
    const buyer = await buyersSchema.findOne({ _id: req.query.id })
    let model
    if (seller) model = await seller
    else if (buyer) model = await buyer
    else if (!seller && !buyer) {
        res.send("Not ok")
    }
    const secret = await model.password + "-" + model.date.getTime()
    jwt.verify(req.query.resetToken, secret, async (err, token) => {
        if (err) {
            res.send("Not ok")
        }
        else {
            res.send('Ok')
        }
    })
})

//Reset the password
app.post('/api/resetmypassword', async (req, res) => {
    const seller = await sellersSchema.findOne({ _id: req.body.id })
    const buyer = await buyersSchema.findOne({ _id: req.body.id })
    let model
    if (seller) model = await sellersSchema
    else if (buyer) model = await buyersSchema
    else if (!seller && !buyer) {
        res.send("Not ok")
    }

    if (req.body.forgotPassword.newPassword === req.body.forgotPassword.confirmNewPassword) {
        try {
            var newEncryptedPassword = await bcrypt.hash(req.body.forgotPassword.newPassword, 10)
        } catch (error) {
            console.log(error)
        }
        model.updateOne({ _id: req.body.id }, { password: newEncryptedPassword })
            .then(data => {
                if (data.nModified === 1) {
                    res.send("Ok")
                }
                else if (data.nModified === 0) {
                    res.send("Error")
                }
            })
    }
    else {
        res.send("Passwords doesn't match")
    }
})

app.post('/api/signup', function (req, res, next) {
    req.body.formDataSignUp.email = req.body.formDataSignUp.email.trim()

    if (!req.body.formDataSignUp.email || !req.body.formDataSignUp.username || !req.body.formDataSignUp.password || !req.body.formDataSignUp.retypePassword || !req.body.formDataSignUp.country || !req.body.formDataSignUp.age || !req.body.formDataSignUp.profilPicture || !req.body.idVerif.idPhoto || !req.body.idVerif.idFace) {
        res.send('A required field is missing')
    }
    else if (req.body.formDataSignUp.country !== "United States") {
        res.send('More countries will be added soon')
    }
    else if (Number.isNaN(req.body.formDataSignUp.age)) {
        res.send('Age value must be a number')
    }
    else if (req.body.formDataSignUp.age < 18) {
        res.send('You have to be at least 18 years old')
    }
    else if (req.body.formDataSignUp.password.length < 6) {
        res.send('Your password must be at least 6 characters')
    }
    else if (req.body.formDataSignUp.retypePassword !== req.body.formDataSignUp.password) {
        res.send('Passwords do not match')
    }
    else if (!emailValidator.validateEmail(req.body.formDataSignUp.email)) {
        res.send('Invalid email address format')
    }
    else if (!req.body.formDataSignUp.termsConditions) {
        res.send('You have to accept the terms and conditions')
    }
    else if (req.body.formDataSignUp.username.length > 14) {
        res.send('Your username can not exceed 14 characters')
    }
    else {
        try {
            buyersSchema.findOne({ $or: [{ email: req.body.formDataSignUp.email }, { username: req.body.formDataSignUp.username }] })
                .select('email')
                .then((member) => {
                    if (member) {
                        if (member.email === req.body.formDataSignUp.email) res.send('This email address already exist')
                        else if (member.username === req.body.formDataSignUp.username) res.send('This username already exist')
                    }
                    if (!member) {
                        sellersSchema.findOne({ $or: [{ email: req.body.formDataSignUp.email }, { username: req.body.formDataSignUp.username }] })
                            .select('email username')
                            .then(async (member) => {
                                if (member) {
                                    if (member.email === req.body.formDataSignUp.email) res.send('This email address already exist')
                                    else if (member.username === req.body.formDataSignUp.username) res.send('This username already exist')
                                }
                                else {
                                    req.body.formDataSignUp.username = req.body.formDataSignUp.username.trim()
                                    req.body.formDataSignUp.password = req.body.formDataSignUp.password.trim()
                                    req.body.formDataSignUp.status = 'seller'
                                    req.body.formDataSignUp.aboutMe = await {
                                        age: req.body.formDataSignUp.age,
                                        country: req.body.formDataSignUp.country,
                                        bio: req.body.formDataSignUp.bio
                                    }
                                    req.body.formDataSignUp.emailPreferences = {
                                        itemPurchased: true,
                                        itemsCommentsModel: true
                                    }
                                    req.body.formDataSignUp.country = ""
                                    bcrypt.hash(req.body.formDataSignUp.password, 10, function (err, hash) {
                                        req.body.formDataSignUp.password = hash
                                        let newMember = new sellersSchema(req.body.formDataSignUp)
                                        newMember.save()
                                            .then(async newMemberData => {
                                                const request = mailjet
                                                    .post("send", { 'version': 'v3.1' })
                                                    .request({
                                                        "Messages": [
                                                            {
                                                                "From": {
                                                                    "Email": "intimy.shop@gmail.com",
                                                                    "Name": "Intimy"
                                                                },
                                                                "To": [
                                                                    {
                                                                        "Email": `${req.body.formDataSignUp.email}`
                                                                    }
                                                                ],
                                                                "Subject": "Intimy account activation",
                                                                "HTMLPart": `Hello ${req.body.formDataSignUp.username},<br />Your Intimy account has been successfully created. Click the button below to activate it and get started. <br /> <button style="border:none;padding:10px;margin-top:30px;margin-bottom:30px;background-color:#FF6D00;"><a href="${process.env.ORIGIN}/activation/${newMemberData._id}" style="color:white;text-decoration:none;">Activate now</a></button><br />Or copy paste the link below<br />${process.env.ORIGIN}/activation/${newMemberData._id}`,
                                                                "CustomID": "AccountVerification"
                                                            }
                                                        ]
                                                    })
                                                request
                                                    .then((result) => {
                                                        console.log(result.body)
                                                    })
                                                    .catch((err) => {
                                                        console.log(err.statusCode)
                                                    })
                                                // Profile picture
                                                let ext = req.body.formDataSignUp.profilPicture.split(';')[0].match(/jpeg|png|gif/)
                                                if (ext) {
                                                    let dataProfilePicture = req.body.formDataSignUp.profilPicture.replace(/^data:image\/\w+;base64,/, "")
                                                    let buffer = new Buffer.from(dataProfilePicture, 'base64')
                                                    let timestamp = new Date().getTime().toString()
                                                    let randomInt = Math.floor((Math.random() * 100) + 1)
                                                    let path = "sellers/" + newMemberData._id + "/profilPictures/" + timestamp + randomInt + "." + ext
                                                    await mkdirp("public/sellers/" + newMemberData._id + "/profilPictures")
                                                    await sharp(buffer)
                                                        .rotate()
                                                        .resize(300, 306)
                                                        .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                                        .jpeg({ quality: 10 })
                                                        .png({ compressionLevel: 9 })
                                                        .toFile('./public/' + path)
                                                    req.body.formDataSignUp.profilPicture = await `${process.env.ORIGIN}/${path}`
                                                }
                                                else {
                                                    errorExtProfilPicture = true
                                                }
                                                // Id photo picture
                                                let extIdPhoto = req.body.idVerif.idPhoto.split(';')[0].match(/jpeg|png|gif/)
                                                if (extIdPhoto) {
                                                    let dataIdPhoto = req.body.idVerif.idPhoto.replace(/^data:image\/\w+;base64,/, "")
                                                    let buffer = new Buffer.from(dataIdPhoto, 'base64')
                                                    let timestamp = new Date().getTime().toString()
                                                    let randomInt = Math.floor((Math.random() * 100) + 1)
                                                    let path = "sellers/" + newMemberData._id + "/idVerif/" + timestamp + randomInt + "." + extIdPhoto
                                                    mkdirp("public/sellers/" + newMemberData._id + "/idVerif/", err => {
                                                        if (err) throw err
                                                        fs.writeFile('public/' + path, buffer, err => {
                                                            if (err) throw err
                                                        })
                                                    })
                                                    req.body.idVerif.idPhoto = `${process.env.ORIGIN}/${path}`
                                                }
                                                else {
                                                    errorExtProfilPicture = true
                                                }
                                                // Id face picture
                                                let extIdFace = req.body.idVerif.idFace.split(';')[0].match(/jpeg|png|gif/)
                                                if (extIdFace) {
                                                    let dataIdFace = req.body.idVerif.idFace.replace(/^data:image\/\w+;base64,/, "")
                                                    let buffer = new Buffer.from(dataIdFace, 'base64')
                                                    let timestamp = new Date().getTime().toString()
                                                    let randomInt = Math.floor((Math.random() * 100) + 1)
                                                    let path = "sellers/" + newMemberData._id + "/idVerif/" + timestamp + randomInt + "." + extIdFace
                                                    mkdirp("public/sellers/" + newMemberData._id + "/idVerif/", err => {
                                                        if (err) throw err
                                                        fs.writeFile('public/' + path, buffer, err => {
                                                            if (err) throw err
                                                        })
                                                    })
                                                    req.body.idVerif.idFace = `${process.env.ORIGIN}/${path}`
                                                }
                                                else {
                                                    errorExtProfilPicture = true
                                                }
                                                const request2 = mailjet
                                                    .post("send", { 'version': 'v3.1' })
                                                    .request({
                                                        "Messages": [
                                                            {
                                                                "From": {
                                                                    "Email": "intimy.shop@gmail.com",
                                                                    "Name": "Intimy ID verification"
                                                                },
                                                                "To": [
                                                                    {
                                                                        "Email": `intimy.shop@gmail.com`
                                                                    }
                                                                ],
                                                                "Subject": "New ID verification request",
                                                                "HTMLPart": `New ID verification request <a href="https://www.intimy.shop/backoffice">Intimy ID verification</a>`,
                                                                "CustomID": "IdVerification"
                                                            }
                                                        ]
                                                    })
                                                request2
                                                    .then((result) => {
                                                        console.log(result.body)
                                                    })
                                                    .catch((err) => {
                                                        console.log(err.statusCode)
                                                    })
                                                await sellersSchema.updateOne({ _id: newMemberData._id }, { $set: { profilPicture: req.body.formDataSignUp.profilPicture, idVerif: req.body.idVerif } })
                                                res.send('Ok')
                                            })
                                            .catch(err => { throw err })
                                    })
                                }
                            })
                    }
                })
        } catch (error) {
            throw error
        }
    }
})

app.post('/api/dataverifsignup', function (req, res) {
    req.body.formDataSignUp.email = req.body.formDataSignUp.email.trim()
    if (!req.body.formDataSignUp.email || !req.body.formDataSignUp.username || !req.body.formDataSignUp.password || !req.body.formDataSignUp.retypePassword) {
        res.send('A required field is missing')
    }
    else if (Number.isNaN(req.body.formDataSignUp.age)) {
        res.send('Age value must be a number')
    }
    else if (req.body.formDataSignUp.age < 18) {
        res.send('You have to be at least 18 years old')
    }
    else if (req.body.formDataSignUp.password.length < 6) {
        res.send('Your password must be at least 6 characters')
    }
    else if (req.body.formDataSignUp.retypePassword !== req.body.formDataSignUp.password) {
        res.send('Passwords do not match')
    }
    else if (!emailValidator.validateEmail(req.body.formDataSignUp.email)) {
        res.send('Invalid email address format')
    }
    else if (!req.body.formDataSignUp.termsConditions) {
        res.send('You have to accept the terms and conditions')
    }
    else if (req.body.formDataSignUp.username.length > 14) {
        res.send('Your username can not exceed 14 characters')
    }
    else {
        try {
            sellersSchema.findOne({ email: req.body.formDataSignUp.email })
                .then(member => {
                    if (member) res.send("Email address already exist")
                    else {
                        buyersSchema.findOne({ email: req.body.formDataSignUp.email })
                            .then(member => {
                                if (member) res.send("Email address already exist")
                                else res.send("Ok")
                            })
                    }
                })
        } catch (error) {
            console.log(error)
        }
    }
})

app.post('/api/signupbuyer', function (req, res) {
    req.body.email = req.body.email.trim()

    if (!req.body.email || !req.body.username || !req.body.password || !req.body.retypePassword) {
        res.send('A required field is missing')
    }
    else if (Number.isNaN(req.body.age)) {
        res.send('Age value must be a number')
    }
    else if (req.body.age < 18) {
        res.send('You have to be at least 18 years old')
    }
    else if (req.body.password.length < 6) {
        res.send('Your password must be at least 6 characters')
    }
    else if (req.body.retypePassword !== req.body.password) {
        res.send('Passwords do not match')
    }
    else if (!emailValidator.validateEmail(req.body.email)) {
        res.send('Invalid email address format')
    }
    else if (!req.body.termsConditions) {
        res.send('You have to accept the terms and conditions')
    }
    else if (req.body.username.length > 14) {
        res.send('Your username can not exceed 14 characters')
    }
    else {
        try {
            sellersSchema.findOne({ email: req.body.email })
                .select('email')
                .exec()
                .then((member) => {
                    if (member) {
                        res.send('This email address already exist')
                    }
                    if (!member) {
                        buyersSchema.findOne({ email: req.body.email })
                            .select('email')
                            .exec()
                            .then((member) => {
                                if (member) {
                                    res.send('This email address already exist')
                                }
                                else {
                                    req.body.profilPicture = `${process.env.ORIGIN}/images/profil_picture.png`
                                    req.body.username = req.body.username.trim()
                                    req.body.password = req.body.password.trim()
                                    req.body.status = 'buyer'

                                    bcrypt.hash(req.body.password, 10, function (err, hash) {
                                        req.body.password = hash
                                        let newMember = new buyersSchema(req.body)
                                        newMember.save()
                                            .then(async newMemberData => {
                                                const request = mailjet
                                                    .post("send", { 'version': 'v3.1' })
                                                    .request({
                                                        "Messages": [
                                                            {
                                                                "From": {
                                                                    "Email": "intimy.shop@gmail.com",
                                                                    "Name": "Intimy"
                                                                },
                                                                "To": [
                                                                    {
                                                                        "Email": `${req.body.email}`,
                                                                        "Name": `${req.body.username}`
                                                                    }
                                                                ],
                                                                "Subject": "Intimy account activation",
                                                                "HTMLPart": `<h3>Hello ${req.body.username},<br />Your Intimy account has been successfully created. Click the button below to activate it and get started. <br /> <button style="border:none;padding:10px;margin-top:30px;margin-bottom:30px;background-color:#FF6D00;"><a href="${process.env.ORIGIN}/activation/${newMemberData._id}" style="color:white;text-decoration:none;">Activate now</a></button><br />Or copy paste the link below<br />${process.env.ORIGIN}/activation/${newMemberData._id}`,
                                                                "CustomID": "BuyerAccountCreated"
                                                            }
                                                        ]
                                                    })
                                                request
                                                    .then((result) => {
                                                        console.log(result.body)
                                                    })
                                                    .catch((err) => {
                                                        console.log(err.statusCode)
                                                    })
                                                res.send('Ok')
                                                mkdirp("public/buyers/" + newMemberData._id + "/", err => {
                                                    if (err) throw err
                                                })
                                            })
                                    })
                                }
                            })
                    }
                })
        } catch (error) {
            throw error
        }
    }
})

app.put('/api/accountactivation', (req, res) => {
    buyersSchema.findOne({ _id: req.body.id })
        .then(member => {
            if (member) {
                if (member.emailVerified === true) {
                    res.send('Account already activated')
                }
                else {
                    member.emailVerified = true
                    member.save()
                        .then(() => {
                            res.send('Buyer account activated')
                        })
                }
            }
            else {
                sellersSchema.findOne({ _id: req.body.id })
                    .then(seller => {
                        if (seller) {
                            if (seller.emailVerified === true) {
                                res.send('Account already activated')
                            }
                            else {
                                seller.emailVerified = true
                                seller.save()
                                    .then(() => {
                                        res.send('Seller account activated')
                                    })
                            }
                        }
                        else res.send('Account already activated')
                    })
            }
        })
})

app.post('/api/getrates/', async (req, res) => {
    try {
        const shippingRates = await shippingRatesModel.find({})
        res.json({ shippingRates })
    } catch (error) {
        console.log(error)
    }
})

app.post('/api/checkout/', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        const { items, shippingRates, fees } = req.body
        let sellersId = []
        let itemsId = []
        let totalPrice = items.reduce(function (a, b) {
            return a + b["price"] + shippingRates + fees
        }, 0)
        if (decoded.userStatus == "buyer") {
            buyersSchema.findById(decoded.userId)
                .then(data => {
                    items.forEach(async item => {
                        let { _id, sellerId } = item
                        sellersId.indexOf(sellerId) ? sellersId.push(sellerId) : null
                        itemsId.indexOf(_id) ? itemsId.push(_id) : null
                    })
                    let serviceCode
                    if (req.body.myAddress.country !== "US") serviceCode = "usps_priority_mail_international"
                    else serviceCode = "usps_priority_mail"
                    produitsSchema.find({ _id: { $in: data.shoppingbag }, bought: true })
                        .then(async produit => {
                            if (produit.length > 0) {
                                res.send("Item not available")
                            }
                            else {
                                if (req.body.saveShippingInfos) {
                                    let { myAddress } = req.body
                                    buyersSchema.updateOne({ '_id': decoded.userId }, { $unset: { 'shoppingbag': 1 }, $set: myAddress }).exec()
                                }
                                else {
                                    buyersSchema.updateOne({ '_id': decoded.userId }, { $unset: { 'shoppingbag': 1 } }).exec()
                                }
                                nsp.to(`notifications/${decoded.userId}`).emit('New notification shopping bag', 'Checkout')
                                const datas = {
                                    "shipment": {
                                        "carrier_id": "se-282199",
                                        "carrier_code": "stamps_com",
                                        "service_code": serviceCode,
                                        "ship_to": {
                                            "name": `${req.body.myAddress.firstname} ${req.body.myAddress.lastname}`,
                                            "address_line1": `${req.body.myAddress.streetaddress} ${req.body.myAddress.apt}`,
                                            "city_locality": req.body.myAddress.city,
                                            "state_province": req.body.myAddress.state,
                                            "postal_code": req.body.myAddress.zipcode,
                                            "country_code": req.body.myAddress.country,
                                            "address_residential_indicator": "yes"
                                        },
                                        "ship_from": {
                                            "name": "Intimy",
                                            "phone": "+13473372136",
                                            "company_name": "Intimy",
                                            "address_line1": "834 Quincy st",
                                            "city_locality": "New York City",
                                            "state_province": "NY",
                                            "postal_code": "11221",
                                            "country_code": "US",
                                            "address_residential_indicator": "no"
                                        },
                                        "customs": {
                                            "contents": "merchandise",
                                            "customs_items": [
                                                {
                                                    "description": "lingerie",
                                                    "quantity": 1,
                                                    "value": 10.0,
                                                    "country_of_origin": "US"
                                                }
                                            ],
                                            "non_delivery": "return_to_sender"
                                        },
                                        "packages": [
                                            {
                                                "package_code": "flat_rate_padded_envelope",
                                                "weight": {
                                                    "value": 10.0,
                                                    "unit": "ounce"
                                                }
                                            }
                                        ]
                                    },
                                    "test_label": true
                                }
                                const headers = {
                                    headers: {
                                        "Content-type": "application/json",
                                        "api-key": "X9K4DSDE+HvtXouVf79I5xjCrC/UwrLdYAXUt6tiT3o"
                                    }
                                }
                                const actualBuyer = await buyersSchema.findOne({ _id: decoded.userId })
                                    .select("customerId")
                                let charge
                                if (!actualBuyer.customerId) {
                                    try {
                                        const customer = await stripe.customers.create({
                                            email: req.body.email,
                                            source: req.body.token
                                        })
                                        charge = await stripe.charges.create({
                                            amount: (req.body.price + req.body.fees + req.body.shippingRates) * 100,
                                            currency: "usd",
                                            description: req.body.description,
                                            customer: customer.id
                                        })
                                        actualBuyer.customerId = customer.id
                                        actualBuyer.save()
                                    } catch (error) {
                                        console.log(error)
                                    }
                                }
                                else {
                                    try {
                                        charge = await stripe.charges.create({
                                            amount: (req.body.price + req.body.fees + req.body.shippingRates) * 100,
                                            currency: "usd",
                                            description: req.body.description,
                                            customer: actualBuyer.customerId
                                        })
                                    } catch (error) {
                                        console.log(error)
                                    }
                                }
                                let purchase = {}
                                let newPurchaseId
                                purchase.sellersId = sellersId
                                purchase.itemsId = itemsId
                                purchase.buyerId = decoded.userId
                                let newPurchase = await new purchasesModel(purchase)
                                await newPurchase.save((err, data) => {
                                    if (err) console.log(err)
                                    newPurchaseId = data._id
                                })
                                axios.post('https://api.shipengine.com/v1/labels', datas, headers)
                                    .then(datas => {
                                        items.forEach(async item => {
                                            let { sellerId, _id, price } = item
                                            if (item.shippingOption === "paddedEnvelope") {
                                                price = await price * (1 - 0.10)
                                            }
                                            produitsSchema.updateMany({ _id: { $in: data.shoppingbag } }, { $set: { bought: true, boughtOn: Date.now(), buyerId: decoded.userId, labelId: datas.data.label_id, shippingLabelLink: datas.data.label_download.href } }, { multi: true })
                                                .exec()
                                            const notification = {
                                                notifType: 'itemBought',
                                                userId: sellerId,
                                                itemBoughtData: {
                                                    buyerId: decoded.userId,
                                                    sellerId: sellerId,
                                                    itemId: _id,
                                                    shippingLabelLink: datas.data.label_download.href
                                                }
                                            }
                                            let newNotification = new notificationsSchema(notification)
                                            newNotification.save()
                                            nsp.to(`notifications/${sellerId}`).emit('New notification')
                                            nsp.to(`notifications/${sellerId}`).emit('Seller tokens added')

                                            const request = mailjet
                                                .post("send", { 'version': 'v3.1' })
                                                .request({
                                                    "Messages": [
                                                        {
                                                            "From": {
                                                                "Email": "intimy.shop@gmail.com",
                                                                "Name": "Intimy order confirmation"
                                                            },
                                                            "To": [
                                                                {
                                                                    "Email": `${data.email}`
                                                                }
                                                            ],
                                                            "Subject": "Order confirmation",
                                                            "HTMLPart": `Hello,<br />Thank you for your order. Your order is confirmed, you will be able to track your package with this tracking address:<br /><a href="https://tools.usps.com/go/TrackConfirmAction.action?tLabels=${datas.data.tracking_number}">${datas.data.tracking_number}</a>`,
                                                            "CustomID": "OrderConfirmation"
                                                        }
                                                    ]
                                                })
                                            request
                                                .then((result) => {
                                                    console.log(result.body)
                                                })
                                                .catch((err) => {
                                                    console.log(err.statusCode)
                                                })

                                            sellersSchema.findOne({ _id: sellerId })
                                                .then(seller => {
                                                    if (seller.emailPreferences.itemPurchased) {
                                                        const request = mailjet
                                                            .post("send", { 'version': 'v3.1' })
                                                            .request({
                                                                "Messages": [
                                                                    {
                                                                        "From": {
                                                                            "Email": "intimy.shop@gmail.com",
                                                                            "Name": "Intimy"
                                                                        },
                                                                        "To": [
                                                                            {
                                                                                "Email": `${seller.email}`
                                                                            }
                                                                        ],
                                                                        "Subject": "Someone bought an item",
                                                                        "HTMLPart": `Hello ${seller.username},<br />Someone bought ${item.name}<br />
                                                                        Go on <a href="${process.env.ORIGIN}">Intimy.shop</a> for more details`,
                                                                        "CustomID": "ItemBought"
                                                                    }
                                                                ]
                                                            })
                                                        request
                                                            .then((result) => {
                                                                console.log(result.body)
                                                            })
                                                            .catch((err) => {
                                                                console.log(err.statusCode)
                                                            })
                                                    }
                                                })
                                            try {
                                                const transfer = stripe.transfers.create({
                                                    amount: item.price * (1 - 0.10) * 100,
                                                    currency: "usd",
                                                    destination: seller.stripeUserId,
                                                    source_transaction: charge.id
                                                }).then(function (transfer) {
                                                    console.log("Transfer:", transfer)
                                                })
                                            } catch (err) {
                                                console.log(err)
                                            }
                                            res.send("Done")
                                        })
                                    }, raison => {
                                        console.log(raison.response.data.errors)
                                        res.send("Error Shipping")
                                    })
                            }
                        })
                })
                .catch(err => { throw err })
        }
        if (decoded.userStatus == "seller") {
            sellersSchema.findById(decoded.userId)
                .then(data => {
                    items.forEach(async item => {
                        let { _id, sellerId } = item
                        sellersId.indexOf(sellerId) ? sellersId.push(sellerId) : null
                        itemsId.indexOf(_id) ? itemsId.push(_id) : null
                    })
                    let serviceCode
                    if (req.body.myAddress.country !== "US") serviceCode = "usps_priority_mail_international"
                    else serviceCode = "usps_priority_mail"
                    produitsSchema.find({ _id: { $in: data.shoppingbag }, bought: true })
                        .then(produit => {
                            if (produit.length > 0) {
                                res.send("Item not available")
                            }
                            else {
                                if (req.body.saveShippingInfos) {
                                    let { myAddress } = req.body
                                    sellersSchema.updateOne({ '_id': decoded.userId }, { $unset: { 'shoppingbag': 1 }, $set: myAddress }).exec()
                                }
                                else {
                                    sellersSchema.updateOne({ '_id': decoded.userId }, { $unset: { 'shoppingbag': 1 } }).exec()
                                }
                                nsp.to(`notifications/${decoded.userId}`).emit('New notification shopping bag', 'Checkout')
                                sellersSchema.find({ _id: { $in: sellersId } })
                                    .then(sellers => {
                                        sellers.map(async seller => {
                                            const datas = {
                                                "shipment": {
                                                    "carrier_id": "se-282199",
                                                    "carrier_code": "stamps_com",
                                                    "service_code": serviceCode,
                                                    "ship_to": {
                                                        "name": `${req.body.myAddress.firstname} ${req.body.myAddress.lastname}`,
                                                        "address_line1": `${req.body.myAddress.streetaddress} ${req.body.myAddress.apt}`,
                                                        "city_locality": req.body.myAddress.city,
                                                        "state_province": req.body.myAddress.state,
                                                        "postal_code": req.body.myAddress.zipcode,
                                                        "country_code": req.body.myAddress.country,
                                                        "address_residential_indicator": "yes"
                                                    },
                                                    "ship_from": {
                                                        "name": "Intimy",
                                                        "phone": "+13473372136",
                                                        "company_name": "Intimy",
                                                        "address_line1": "834 Quincy st",
                                                        "city_locality": "New York City",
                                                        "state_province": "NY",
                                                        "postal_code": "11221",
                                                        "country_code": "US",
                                                        "address_residential_indicator": "no"
                                                    },
                                                    "customs": {
                                                        "contents": "merchandise",
                                                        "customs_items": [
                                                            {
                                                                "description": "lingerie",
                                                                "quantity": 1,
                                                                "value": 10.0,
                                                                "country_of_origin": "US"
                                                            }
                                                        ],
                                                        "non_delivery": "return_to_sender"
                                                    },
                                                    "packages": [
                                                        {
                                                            "package_code": "flat_rate_padded_envelope",
                                                            "weight": {
                                                                "value": 10.0,
                                                                "unit": "ounce"
                                                            }
                                                        }
                                                    ]
                                                },
                                                "test_label": true
                                            }
                                            const headers = {
                                                'headers': {
                                                    "Content-type": "application/json",
                                                    "api-key": "X9K4DSDE+HvtXouVf79I5xjCrC/UwrLdYAXUt6tiT3o"
                                                }
                                            }
                                            const actualBuyer = await sellersSchema.findOne({ _id: decoded.userId })
                                                .select("customerId")
                                            let charge
                                            if (!actualBuyer.customerId) {
                                                try {
                                                    const customer = await stripe.customers.create({
                                                        email: req.body.email,
                                                        source: req.body.token
                                                    })
                                                    charge = await stripe.charges.create({
                                                        amount: (req.body.price + req.body.fees + req.body.shippingRates) * 100,
                                                        currency: "usd",
                                                        description: req.body.description,
                                                        customer: customer.id
                                                    })
                                                    actualBuyer.customerId = customer.id
                                                    actualBuyer.save()
                                                } catch (error) {
                                                    console.log(error)
                                                }
                                            }
                                            else {
                                                try {
                                                    charge = await stripe.charges.create({
                                                        amount: (req.body.price + req.body.fees + req.body.shippingRates) * 100,
                                                        currency: "usd",
                                                        description: req.body.description,
                                                        customer: actualBuyer.customerId
                                                    })
                                                } catch (error) {
                                                    console.log(error)
                                                }
                                            }
                                            let purchase = {}
                                            let newPurchaseId
                                            purchase.sellersId = sellersId
                                            purchase.itemsId = itemsId
                                            purchase.buyerId = decoded.userId
                                            let newPurchase = await new purchasesModel(purchase)
                                            await newPurchase.save((err, data) => {
                                                if (err) console.log(err)
                                                newPurchaseId = data._id
                                            })
                                            axios.post('https://api.shipengine.com/v1/labels', datas, headers)
                                                .then(datas => {
                                                    items.forEach(async item => {
                                                        let { sellerId, _id, price } = item
                                                        if (item.shippingOption === "paddedEnvelope") {
                                                            price = await price * (1 - 0.10)
                                                        }
                                                        produitsSchema.updateMany({ _id: { $in: data.shoppingbag } }, { $set: { bought: true, boughtOn: Date.now(), buyerId: decoded.userId, labelId: datas.data.label_id, shippingLabelLink: datas.data.label_download.href } }, { multi: true })
                                                            .exec()
                                                        const notification = {
                                                            notifType: 'itemBought',
                                                            userId: sellerId,
                                                            itemBoughtData: {
                                                                buyerId: decoded.userId,
                                                                sellerId: sellerId,
                                                                itemId: _id,
                                                                shippingLabelLink: datas.data.label_download.href
                                                            }
                                                        }
                                                        let newNotification = new notificationsSchema(notification)
                                                        newNotification.save()
                                                        nsp.to(`notifications/${sellerId}`).emit('New notification')
                                                        nsp.to(`notifications/${sellerId}`).emit('Seller tokens added')
                                                        const request = mailjet
                                                            .post("send", { 'version': 'v3.1' })
                                                            .request({
                                                                "Messages": [
                                                                    {
                                                                        "From": {
                                                                            "Email": "intimy.shop@gmail.com",
                                                                            "Name": "Intimy order confirmation"
                                                                        },
                                                                        "To": [
                                                                            {
                                                                                "Email": `${data.email}`
                                                                            }
                                                                        ],
                                                                        "Subject": "Order confirmation",
                                                                        "HTMLPart": `Hello,<br />Thank you for your order. Your order is confirmed, you will be able to track your package with this tracking address:<br /><a href="https://tools.usps.com/go/TrackConfirmAction.action?tLabels=${datas.data.tracking_number}">${datas.data.tracking_number}</a>`,
                                                                        "CustomID": "OrderConfirmation"
                                                                    }
                                                                ]
                                                            })
                                                        request
                                                            .then((result) => {
                                                                console.log(result.body)
                                                            })
                                                            .catch((err) => {
                                                                console.log(err.statusCode)
                                                            })
                                                        sellersSchema.findOne({ _id: sellerId })
                                                            .then(seller => {
                                                                if (seller.emailPreferences.itemPurchased) {
                                                                    const request = mailjet
                                                                        .post("send", { 'version': 'v3.1' })
                                                                        .request({
                                                                            "Messages": [
                                                                                {
                                                                                    "From": {
                                                                                        "Email": "intimy.shop@gmail.com",
                                                                                        "Name": "Intimy"
                                                                                    },
                                                                                    "To": [
                                                                                        {
                                                                                            "Email": `${seller.email}`
                                                                                        }
                                                                                    ],
                                                                                    "Subject": "Someone bought an item",
                                                                                    "HTMLPart": `Hello ${seller.username},<br />Someone bought ${item.name}<br />
                                                                                    Go on <a href="${process.env.ORIGIN}">Intimy.shop</a> for more details`,
                                                                                    "CustomID": "ItemBought"
                                                                                }
                                                                            ]
                                                                        })
                                                                    request
                                                                        .then((result) => {
                                                                            console.log(result.body)
                                                                        })
                                                                        .catch((err) => {
                                                                            console.log(err.statusCode)
                                                                        })
                                                                }
                                                            })
                                                        try {
                                                            const transfer = stripe.transfers.create({
                                                                amount: item.price * (1 - 0.10) * 100,
                                                                currency: "usd",
                                                                destination: seller.stripeUserId,
                                                                source_transaction: charge.id
                                                            }).then(function (transfer) {
                                                                console.log("Transfer:", transfer)
                                                            })
                                                        } catch (err) {
                                                            console.log(err)
                                                        }
                                                        res.send("Done")
                                                    })
                                                }, raison => {
                                                    console.log(raison.response.data.errors)
                                                    res.send("Error Shipping")
                                                })
                                        })
                                    })
                            }
                        })
                })
                .catch(err => { throw err })
        }
    })
})

app.post("/api/picturessetcheckout", async (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (decoded.userStatus === "buyer") {
            buyersSchema.findOne({ _id: decoded.userId })
                .then(buyer => {
                    produitsPicturesModel.findOne({ _id: req.body.itemId })
                        .then(async produit => {
                            if (buyer.tokens >= Math.ceil(produit.price + (produit.price * 0.10))) {
                                try {
                                    buyer.tokens = await buyer.tokens - Math.ceil((produit.price + (produit.price * 0.10)))
                                    buyer.save()
                                    sellersSchema.updateOne({ _id: produit.sellerId }, { $inc: { tokens: Math.ceil(produit.price - (produit.price * 0.10)) } }).exec()
                                    if (!produit.boughtBy.includes(String(buyer._id))) {
                                        produit.boughtBy.push(buyer._id)
                                        produit.lastSale = Date.now()
                                        produit.save()
                                    }
                                    const notification = {
                                        notifType: 'itemPicturesBought',
                                        userId: produit.sellerId,
                                        itemBoughtData: {
                                            buyerId: buyer._id,
                                            sellerId: produit.sellerId,
                                            itemId: produit._id
                                        }
                                    }
                                    let newNotification = new notificationsSchema(notification)
                                    newNotification.save()
                                    nsp.to(`notifications/${produit.sellerId}`).emit('New notification')
                                    nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', Math.ceil(produit.price + (produit.price * 0.10)))
                                    nsp.to(`notifications/${produit.sellerId}`).emit('Seller tokens added')
                                    res.send(produit.pictures)
                                } catch (error) {
                                    console.log(error)
                                }
                            }
                            else {
                                res.send('Not enough tokens')
                            }
                        })
                })
        }
        else if (decoded.userStatus === "seller") {
            sellersSchema.findOne({ _id: decoded.userId })
                .then(seller => {
                    produitsPicturesModel.findOne({ _id: req.body.itemId })
                        .then(async produit => {
                            if (seller.tokens >= Math.ceil(produit.price + (produit.price * 0.10))) {
                                try {
                                    seller.tokens = await seller.tokens - Math.ceil((produit.price + (produit.price * 0.10)))
                                    seller.save()
                                    sellersSchema.updateOne({ _id: produit.sellerId }, { $inc: { tokens: Math.ceil(produit.price - (produit.price * 0.10)) } }).exec()
                                    if (!produit.boughtBy.includes(String(seller._id))) {
                                        produit.boughtBy.push(seller._id)
                                        produit.lastSale = Date.now()
                                        produit.save()
                                    }
                                    const notification = {
                                        notifType: 'itemPicturesBought',
                                        userId: produit.sellerId,
                                        itemBoughtData: {
                                            buyerId: seller._id,
                                            sellerId: produit.sellerId,
                                            itemId: produit._id
                                        }
                                    }
                                    let newNotification = new notificationsSchema(notification)
                                    newNotification.save()
                                    nsp.to(`notifications/${produit.sellerId}`).emit('New notification')
                                    nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', Math.ceil(produit.price + (produit.price * 0.10)))
                                    nsp.to(`notifications/${produit.sellerId}`).emit('Seller tokens added')
                                    res.send(produit.pictures)
                                } catch (error) {
                                    console.log(error)
                                }
                            }
                            else {
                                res.send('Not enough tokens')
                            }
                        })
                })
        }
    })
})

app.post("/api/videossetcheckout", async (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        if (decoded.userStatus === "buyer") {
            buyersSchema.findOne({ _id: decoded.userId })
                .then(buyer => {
                    produitsVideosModel.findOne({ _id: req.body.itemId })
                        .then(async produit => {
                            if (buyer.tokens >= Math.ceil(produit.price + (produit.price * 0.10))) {
                                try {
                                    buyer.tokens = await buyer.tokens - Math.ceil((produit.price + (produit.price * 0.10)))
                                    buyer.save()
                                    sellersSchema.updateOne({ _id: produit.sellerId }, { $inc: { tokens: Math.ceil(produit.price - (produit.price * 0.10)) } }).exec()
                                    if (!produit.boughtBy.includes(String(buyer._id))) {
                                        produit.boughtBy.push(buyer._id)
                                        produit.lastSale = Date.now()
                                        produit.save()
                                    }
                                    const notification = {
                                        notifType: 'itemVideosBought',
                                        userId: produit.sellerId,
                                        itemBoughtData: {
                                            buyerId: buyer._id,
                                            sellerId: produit.sellerId,
                                            itemId: produit._id
                                        }
                                    }
                                    let newNotification = new notificationsSchema(notification)
                                    newNotification.save()
                                    nsp.to(`notifications/${produit.sellerId}`).emit('New notification')
                                    nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', Math.ceil(produit.price + (produit.price * 0.10)))
                                    nsp.to(`notifications/${produit.sellerId}`).emit('Seller tokens added')
                                    res.send(produit.pictures)
                                } catch (error) {
                                    console.log(error)
                                }
                            }
                            else {
                                res.send('Not enough tokens')
                            }
                        })
                })
        }
        else if (decoded.userStatus === "seller") {
            sellersSchema.findOne({ _id: decoded.userId })
                .then(seller => {
                    produitsVideosModel.findOne({ _id: req.body.itemId })
                        .then(async produit => {
                            if (seller.tokens >= Math.ceil(produit.price + (produit.price * 0.10))) {
                                try {
                                    seller.tokens = await seller.tokens - Math.ceil((produit.price + (produit.price * 0.10)))
                                    seller.save()
                                    sellersSchema.updateOne({ _id: produit.sellerId }, { $inc: { tokens: Math.ceil(produit.price - (produit.price * 0.10)) } }).exec()
                                    if (!produit.boughtBy.includes(String(seller._id))) {
                                        produit.boughtBy.push(seller._id)
                                        produit.lastSale = Date.now()
                                        produit.save()
                                    }
                                    const notification = {
                                        notifType: 'itemVideosBought',
                                        userId: produit.sellerId,
                                        itemBoughtData: {
                                            buyerId: seller._id,
                                            sellerId: produit.sellerId,
                                            itemId: produit._id
                                        }
                                    }
                                    let newNotification = new notificationsSchema(notification)
                                    newNotification.save()
                                    nsp.to(`notifications/${produit.sellerId}`).emit('New notification')
                                    nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', Math.ceil(produit.price + (produit.price * 0.10)))
                                    nsp.to(`notifications/${produit.sellerId}`).emit('Seller tokens added')
                                    res.send(produit.pictures)
                                } catch (error) {
                                    console.log(error)
                                }
                            }
                            else {
                                res.send('Not enough tokens')
                            }
                        })
                })
        }
    })
})

app.post('/api/directsellcheckout/', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        const { item, data } = req.body
        let totalPriceTokens = item.reduce(function (a, b) {
            return a + b["price"] + data.shippingRates + data.fees
        }, 0)
        if (decoded.userStatus == "buyer") {
            buyersSchema.findById(decoded.userId)
                .then(data => {
                    if (data.tokens >= totalPriceTokens) {
                        directSellProductsModel.find({ _id: item[0]._id, bought: true })
                            .then(produit => {
                                if (produit.length > 0) {
                                    res.send("Item not available")
                                }
                                else {
                                    if (req.body.data.saveShippingInfos) {
                                        let { myAddress } = req.body.data
                                        buyersSchema.updateOne({ '_id': decoded.userId }, { $inc: { tokens: -totalPriceTokens }, $set: myAddress }).exec()
                                    }
                                    else {
                                        buyersSchema.updateOne({ '_id': decoded.userId }, { $inc: { tokens: -totalPriceTokens } }).exec()
                                    }
                                    nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', totalPriceTokens)
                                    const datas = {
                                        "shipment": {
                                            "carrier_id": "se-282199",
                                            "carrier_code": "stamps_com",
                                            "service_code": "usps_priority_mail",
                                            "ship_to": {
                                                "name": `${data.firstname} ${data.lastname}`,
                                                "address_line1": `${data.streetaddress} ${data.apt}`,
                                                "city_locality": data.city,
                                                "state_province": data.state,
                                                "postal_code": data.zipcode,
                                                "country_code": "US",
                                                "address_residential_indicator": "No"
                                            },
                                            "ship_from": {
                                                "name": "Intimy",
                                                "phone": "+13473372136",
                                                "company_name": "Intimy",
                                                "address_line1": "834 Quincy st",
                                                "city_locality": "New York City",
                                                "state_province": "NY",
                                                "postal_code": "11221",
                                                "country_code": "US",
                                                "address_residential_indicator": "no"
                                            },
                                            "packages": [
                                                {
                                                    "package_code": "flat_rate_padded_envelope",
                                                    "weight": {
                                                        "value": 1.0,
                                                        "unit": "ounce"
                                                    }
                                                }
                                            ]
                                        },
                                        "test_label": true
                                    }
                                    const headers = {
                                        headers: {
                                            "Content-type": "application/json",
                                            "api-key": "X9K4DSDE+HvtXouVf79I5xjCrC/UwrLdYAXUt6tiT3o"
                                        }
                                    }
                                    axios.post('https://api.shipengine.com/v1/labels', datas, headers)
                                        .then(async datas => {
                                            item.forEach(async item => {
                                                let { sellerId, _id, price } = item
                                                if (item.shippingOption === "paddedEnvelope") {
                                                    price = await price * (1 - 0.10)
                                                }
                                                directSellProductsModel.updateMany({ _id: item[0]._id }, { $set: { bought: true, boughtOn: Date.now(), buyerId: decoded.userId, labelId: datas.data.label_id } })
                                                    .exec()
                                                sellersSchema.updateOne({ _id: sellerId }, { $inc: { tokens: Math.ceil(price) } }).exec()
                                                const notification = {
                                                    notifType: 'itemBought',
                                                    userId: sellerId,
                                                    itemBoughtData: {
                                                        buyerId: decoded.userId,
                                                        sellerId: sellerId,
                                                        itemId: _id,
                                                        shippingLabelLink: datas.data.label_download.href
                                                    }
                                                }
                                                let newNotification = await new notificationsSchema(notification)
                                                newNotification.save()
                                                nsp.to(`notifications/${sellerId}`).emit('New notification')
                                                nsp.to(`notifications/${sellerId}`).emit('Seller tokens added')
                                                nspLiveDirectSell.to(`LiveDirectSell/${sellerId}`).emit('Direct sell item sold')
                                                const request = mailjet
                                                    .post("send", { 'version': 'v3.1' })
                                                    .request({
                                                        "Messages": [
                                                            {
                                                                "From": {
                                                                    "Email": "intimy.shop@gmail.com",
                                                                    "Name": "Intimy order confirmation"
                                                                },
                                                                "To": [
                                                                    {
                                                                        "Email": `${data.email}`
                                                                    }
                                                                ],
                                                                "Subject": "Order confirmation",
                                                                "HTMLPart": `Hello,<br />Thank you for your order. Your order is confirmed, you will be able to track your package with this tracking address:<br /><a href="https://tools.usps.com/go/TrackConfirmAction.action?tLabels=${datas.data.tracking_number}">${datas.data.tracking_number}</a>`,
                                                                "CustomID": "OrderConfirmation"
                                                            }
                                                        ]
                                                    })
                                                request
                                                    .then((result) => {
                                                        console.log(result.body)
                                                    })
                                                    .catch((err) => {
                                                        console.log(err.statusCode)
                                                    })
                                                res.send("Done")
                                            }, raison => {
                                                console.log(raison.response.data.errors)
                                                res.send("Error Shipping")
                                            })
                                        })
                                }
                            })
                    }
                    else {
                        res.send("Not enough tokes")
                    }
                })
                .catch(err => { throw err })
        }
        if (decoded.userStatus == "seller") {
            sellersSchema.findById(decoded.userId)
                .then(data => {
                    if (data.tokens >= totalPriceTokens) {
                        directSellProductsModel.find({ _id: item[0]._id, bought: true })
                            .then(produit => {
                                if (produit.length > 0) {
                                    res.send("Item not available")
                                }
                                else {
                                    if (req.body.data.saveShippingInfos) {
                                        let { myAddress } = req.body.data
                                        sellersSchema.updateOne({ '_id': decoded.userId }, { $inc: { tokens: -totalPriceTokens }, $set: myAddress }).exec()
                                    }
                                    else {
                                        sellersSchema.updateOne({ '_id': decoded.userId }, { $inc: { tokens: -totalPriceTokens } }).exec()
                                    }
                                    nsp.to(`notifications/${decoded.userId}`).emit('Tokens substract', totalPriceTokens)
                                    sellersSchema.find({ _id: item[0].sellerId })
                                        .then(sellers => {
                                            sellers.map(seller => {
                                                const datas = {
                                                    "shipment": {
                                                        "carrier_id": "se-282199",
                                                        "carrier_code": "stamps_com",
                                                        "service_code": "usps_priority_mail",
                                                        "ship_to": {
                                                            "name": `${data.firstname} ${data.lastname}`,
                                                            "address_line1": `${data.streetaddress} ${data.apt}`,
                                                            "city_locality": data.city,
                                                            "state_province": data.state,
                                                            "postal_code": data.zipcode,
                                                            "country_code": "US",
                                                            "address_residential_indicator": "No"
                                                        },
                                                        "ship_from": {
                                                            "name": "Intimy",
                                                            "phone": "+13473372136",
                                                            "company_name": "Intimy",
                                                            "address_line1": "834 Quincy st",
                                                            "city_locality": "New York City",
                                                            "state_province": "NY",
                                                            "postal_code": "11221",
                                                            "country_code": "US",
                                                            "address_residential_indicator": "no"
                                                        },
                                                        "packages": [
                                                            {
                                                                "package_code": "flat_rate_padded_envelope",
                                                                "weight": {
                                                                    "value": 1.0,
                                                                    "unit": "ounce"
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    "test_label": true
                                                }
                                                const headers = {
                                                    'headers': {
                                                        "Content-type": "application/json",
                                                        "api-key": "X9K4DSDE+HvtXouVf79I5xjCrC/UwrLdYAXUt6tiT3o"
                                                    }
                                                }
                                                axios.post('https://api.shipengine.com/v1/labels', datas, headers)
                                                    .then(async datas => {
                                                        item.forEach(async item => {
                                                            let { sellerId, _id, price } = item
                                                            if (item.shippingOption === "paddedEnvelope") {
                                                                price = await price * (1 - 0.10)
                                                            }
                                                            directSellProductsModel.updateMany({ _id: item[0]._id }, { $set: { bought: true, boughtOn: Date.now(), buyerId: decoded.userId, labelId: datas.data.label_id } })
                                                                .exec()
                                                            sellersSchema.updateOne({ _id: sellerId }, { $inc: { tokens: Math.ceil(price) } }).exec()
                                                            const notification = {
                                                                notifType: 'itemBought',
                                                                userId: sellerId,
                                                                itemBoughtData: {
                                                                    buyerId: decoded.userId,
                                                                    sellerId: sellerId,
                                                                    itemId: _id,
                                                                    shippingLabelLink: datas.data.label_download.href
                                                                }
                                                            }
                                                            let newNotification = await new notificationsSchema(notification)
                                                            newNotification.save()
                                                            nsp.to(`notifications/${sellerId}`).emit('New notification')
                                                            nsp.to(`notifications/${sellerId}`).emit('Seller tokens added')
                                                            nspLiveDirectSell.to(`LiveDirectSell/${sellerId}`).emit('Direct sell item sold')
                                                            const request = mailjet
                                                                .post("send", { 'version': 'v3.1' })
                                                                .request({
                                                                    "Messages": [
                                                                        {
                                                                            "From": {
                                                                                "Email": "intimy.shop@gmail.com",
                                                                                "Name": "Intimy order confirmation"
                                                                            },
                                                                            "To": [
                                                                                {
                                                                                    "Email": `${data.email}`
                                                                                }
                                                                            ],
                                                                            "Subject": "Order confirmation",
                                                                            "HTMLPart": `Hello,<br />Thank you for your order. Your order is confirmed, you will be able to track your package with this tracking address:<br /><a href="https://tools.usps.com/go/TrackConfirmAction.action?tLabels=${datas.data.tracking_number}">${datas.data.tracking_number}</a>`,
                                                                            "CustomID": "OrderConfirmation"
                                                                        }
                                                                    ]
                                                                })
                                                            request
                                                                .then((result) => {
                                                                    console.log(result.body)
                                                                })
                                                                .catch((err) => {
                                                                    console.log(err.statusCode)
                                                                })
                                                            res.send("Done")
                                                        }, raison => {
                                                            console.log(raison.response.data.errors)
                                                            res.send("Error Shipping")
                                                        })
                                                    })
                                            })
                                        })
                                }
                            })
                    }
                    else {
                        res.send("Not enough tokens")
                    }
                })
                .catch(err => { throw err })
        }
    })
})

//Add post on item's page
app.post('/api/additemcomment', function (req, res) {
    if (req.body.itemId, req.body.comment) {
        jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
            sellersSchema.findOne({ _id: req.body.sellerId })
                .then(async seller => {
                    const item = await produitsSchema.findOne({ _id: req.body.itemId }).select('_id name').exec()
                    let model
                    if (decoded.userStatus == "seller") model = sellersSchema
                    else if (decoded.userStatus == "buyer") model = buyersSchema
                    model.findOne({ _id: decoded.userId })
                        .then(member => {
                            let comment = {}
                            comment.userId = decoded.userId
                            comment.username = member.username
                            comment.itemId = req.body.itemId
                            comment.userIp = req.connection.remoteAddress
                            comment.comment = req.body.comment
                            const newComment = new itemsCommentsModel(comment)
                            newComment.save()
                                .then(data => {
                                    if (seller.emailPreferences.itemCommentPosted && decoded.userId != seller._id) {
                                        const request = mailjet
                                            .post("send", { 'version': 'v3.1' })
                                            .request({
                                                "Messages": [
                                                    {
                                                        "From": {
                                                            "Email": "intimy.shop@gmail.com",
                                                            "Name": "Intimy "
                                                        },
                                                        "To": [
                                                            {
                                                                "Email": `${seller.email}`
                                                            }
                                                        ],
                                                        "Subject": "New item comment",
                                                        "HTMLPart": `<div>Hello ${seller.username}</div>, <div style="margin-top:30px;">A new comment has been posted on your item "${item.name}".</div>
                                                        <div style="margin-top:30px;margin-bottom:30px;>You can access it directly from here <a href="${process.env.ORIGIN}/item/${item._id}">${item.name}</a></div>`,
                                                        "CustomID": "ItemComment"
                                                    }
                                                ]
                                            })
                                        request
                                            .then((result) => {
                                                console.log(result.body)
                                            })
                                            .catch((err) => {
                                                console.log(err.statusCode)
                                            })
                                    }
                                    res.json({ status: "Comment added", comment: data })
                                })
                        })
                })
        })
    }
})

//Get posts on user's profile
app.get('/api/getitemcomments', function (req, res) {
    if (req.query.itemId) {
        itemsCommentsModel.find({ itemId: req.query.itemId })
            .then(comments => {
                res.json(comments)
            })
    }
})

app.post('/api/transcoded', (req, res, next) => {
    sellersSchema.findOne({ _id: req.body.seller }, (err, seller) => {
        let ext = req.body.fileName.split(';')[0].match(/mp4|avi|mov|wmv|ogg/)
        if (ext) {
            let path = process.env.URL_TRANSCODING_SERVER + "/intimy/sellers/" + req.body.seller + "/profilVideos/" + req.body.fileName + "." + ext
            let pathMin = "intimy/sellers/" + req.body.seller + "/profilVideos/thumbnails/" + req.body.fileName + "/img_05.jpg"
            seller.pictures.push({ src: `${path}`, thumbnail: `${process.env.URL_TRANSCODING_SERVER}/${pathMin}`, kind: "video" })
            seller.save()
            res.send("Ok")
        }
        else {
            errorExtProfilPicture = true
        }
    })
})

app.post('/api/videosetadded', async (req, res, next) => {
    let path = process.env.URL_TRANSCODING_SERVER + "/intimy/sellers/" + req.body.seller + "/videosSets/" + req.body.fileName
    let newItem = {}
    newItem.name = req.body.formDataVideos.name
    newItem.description = req.body.formDataVideos.description
    newItem.price = Number(req.body.formDataVideos.price)
    newItem.sellerId = req.body.seller
    newItem.img = `${process.env.ORIGIN}/images/videosSet.svg`
    newItem.numberVideos = req.body.videosLength.length
    newItem.videos = path
    await sellersSchema.updateOne({ _id: req.body.seller }, { $set: { lastActivity: Date.now() } }, { upsert: true })
    let produit = new produitsVideosModel(newItem)
    produit.save()
        .then(() => {
            nspPostProduit.emit('Produit posted')
            res.send("Your item is online!")
        })
})

app.put('/api/modifyMyProfile', (req, res, next) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        if (decoded.userStatus == "seller") {
            sellersSchema.findOne({ username: req.body.editedProfil.username }, (err, sellers) => {
                if (sellers && decoded.userId != sellers._id) {
                    res.send('Username already exist')
                    return next('Username already exist')
                }
                else {
                    buyersSchema.findOne({ username: req.body.editedProfil.username }, (err, buyers) => {
                        if (buyers && decoded.userId != buyers._id) {
                            res.send('Username already exist')
                            return next('Username already exist')
                        }
                        else {
                            sellersSchema.findById(decoded.userId, async (err, seller) => {
                                let errorExtProfilPicture
                                if (req.body.editedProfil.username) seller.username = req.body.editedProfil.username
                                if (req.body.editedProfil.bio) seller.aboutMe.bio = req.body.editedProfil.bio
                                if (req.body.editedProfil.hairs) seller.aboutMe.hairs = req.body.editedProfil.hairs
                                if (req.body.editedProfil.height) seller.aboutMe.height = req.body.editedProfil.height
                                if (req.body.editedProfil.weight) seller.aboutMe.weight = req.body.editedProfil.weight
                                if (req.body.editedProfil.eyes) seller.aboutMe.eyes = req.body.editedProfil.eyes
                                if (req.body.editedProfil.status) seller.aboutMe.status = req.body.editedProfil.status
                                if (req.body.editedProfil.interestedIn) seller.aboutMe.interestedIn = req.body.editedProfil.interestedIn
                                if (req.body.editedProfil.measurements) seller.aboutMe.measurements = req.body.editedProfil.measurements
                                if (req.body.editedProfil.tattoos) seller.aboutMe.tattoos = req.body.editedProfil.tattoos
                                if (req.body.editedProfil.piercings) seller.aboutMe.piercings = req.body.editedProfil.piercings
                                if (req.body.editedProfil.whatILike) seller.aboutMe.whatILike = req.body.editedProfil.whatILike
                                if (req.body.editedProfil.whatIDontLike) seller.aboutMe.whatIDontLike = req.body.editedProfil.whatIDontLike
                                seller.emailPreferences.itemPurchased = req.body.editedProfil.itemPurchased
                                seller.emailPreferences.itemCommentPosted = req.body.editedProfil.itemCommentPosted
                                if (req.body.editedProfil.profilPicture) {
                                    await forEach(req.body.editedProfil.profilPicture, async profilPicture => {
                                        let ext = profilPicture.split(';')[0].match(/jpeg|png/)
                                        if (ext) {
                                            let data = profilPicture.replace(/^data:image\/\w+;base64,/, "")
                                            let buffer = new Buffer.from(data, 'base64')
                                            let timestamp = new Date().getTime().toString()
                                            let randomInt = Math.floor((Math.random() * 100) + 1)
                                            let path = "sellers/" + decoded.userId + "/profilPictures/" + timestamp + randomInt + "." + ext
                                            await mkdirp("public/sellers/" + decoded.userId + "/profilPictures/")
                                            await sharp(buffer)
                                                .rotate()
                                                .resize(300, 306)
                                                .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                                .jpeg({ quality: 10 })
                                                .png({ compressionLevel: 9 })
                                                .toFile('public/' + path)
                                            seller.profilPicture = `${process.env.ORIGIN}/${path}`
                                        }
                                        else {
                                            errorExtProfilPicture = true
                                        }
                                    })
                                }
                                if (req.body.editedProfil.pictures) {
                                    await forEach(req.body.editedProfil.pictures, async pictures => {
                                        let ext = pictures.split(';')[0].match(/jpeg|png/)
                                        if (ext) {
                                            let data = pictures.replace(/^data:image\/\w+;base64,/, "")
                                            let buffer = new Buffer.from(data, 'base64')
                                            let timestamp = new Date().getTime().toString()
                                            let randomInt = Math.floor((Math.random() * 100) + 1)
                                            let path = "sellers/" + decoded.userId + "/pictures/" + timestamp + randomInt + "." + ext
                                            let pathMin = "sellers/" + decoded.userId + "/pictures/min/" + timestamp + randomInt + "-min." + ext
                                            await mkdirp("public/sellers/" + decoded.userId + "/pictures/min/")
                                            const sharpPictures = await sharp(buffer)
                                                .withMetadata()
                                                .rotate()
                                                .resize(800, 1000, { fit: "inside" })
                                                .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                                .jpeg({ quality: 10 })
                                                .png({ compressionLevel: 9 })
                                                .toFile('public/' + path)
                                            await sharp(buffer)
                                                .rotate()
                                                .resize(300, 306)
                                                .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                                .jpeg({ quality: 10 })
                                                .png({ compressionLevel: 9 })
                                                .toFile('public/' + pathMin)
                                            seller.pictures.push({ src: `${process.env.ORIGIN}/${path}`, thumbnail: `${process.env.ORIGIN}/${pathMin}`, w: sharpPictures.width, h: sharpPictures.height })
                                        }
                                        else {
                                            errorExtProfilPicture = true
                                        }
                                    })
                                }
                                if (req.body.editedProfil.username.length > 14) {
                                    res.send('Your username can not exceed 14 characters')
                                    return next('Your username can not exceed 14 characters')
                                }
                                else if (req.body.editedProfil.bio.length > 140) {
                                    res.send('Your bio can not exceed 140 characters')
                                    return next('Your bio can not exceed 140 characters')
                                }
                                else seller.save()
                                    .then(() => {
                                        if (errorExtProfilPicture) res.send("Wrong ext")
                                        else res.send("Profile updated!")
                                    })
                            })
                        }
                    })
                }
            })
        }
        if (decoded.userStatus == "buyer") {
            sellersSchema.findOne({ username: req.body.editedProfil.username }, (err, sellers) => {
                if (sellers && decoded.userId != sellers._id) {
                    res.send('Username already exist')
                    return next('Username already exist')
                }
                else {
                    buyersSchema.findOne({ username: req.body.editedProfil.username }, async (err, buyers) => {
                        if (buyers && decoded.userId != buyers._id) {
                            res.send('Username already exist')
                            return next('Username already exist')
                        }
                        else {
                            let updatedProfil = {}
                            if (req.body.editedProfil.username) updatedProfil.username = req.body.editedProfil.username
                            if (req.body.editedProfil.profilPicture) {
                                await forEach(req.body.editedProfil.profilPicture, async profilPicture => {
                                    let ext = profilPicture.split(';')[0].match(/jpeg|png/)
                                    if (ext) {
                                        let data = profilPicture.replace(/^data:image\/\w+;base64,/, "")
                                        let buffer = new Buffer.from(data, 'base64')
                                        let timestamp = new Date().getTime().toString()
                                        let randomInt = Math.floor((Math.random() * 100) + 1)
                                        let path = "buyers/" + decoded.userId + "/profilPictures/" + timestamp + randomInt + "." + ext
                                        await mkdirp("public/buyers/" + decoded.userId + "/profilPictures/")
                                        await sharp(buffer)
                                            .rotate()
                                            .resize(300, 306)
                                            .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                            .jpeg({ quality: 10 })
                                            .png({ compressionLevel: 9 })
                                            .toFile('public/' + path)
                                        updatedProfil.profilPicture = `${process.env.ORIGIN}/${path}`
                                    }
                                    else {
                                        errorExtProfilPicture = true
                                    }
                                })
                            }
                            if (req.body.editedProfil.username.length > 14) {
                                res.send('Your username can not exceed 14 characters')
                            }
                            else {
                                buyersSchema.updateOne({ _id: decoded.userId }, { $set: updatedProfil }, (err, profil) => {
                                    if (err) throw err
                                    res.send("Profile updated!")
                                })
                            }
                        }
                    })
                }
            })
        }
    })
})

app.put('/api/modifymyshippingdetails', (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {

        if (req.body.firstname && req.body.lastname && req.body.streetaddress && req.body.city && req.body.zipcode) {
            if (decoded.userStatus === "buyer") {
                buyersSchema.findById(decoded.userId, async (err, seller) => {
                    buyersSchema.updateOne({ _id: decoded.userId }, { $set: req.body }, (err, profil) => {
                        if (err) throw err
                        res.send("Shipping details updated!")
                    })
                })
            }
            else {
                sellersSchema.findById(decoded.userId, async (err, seller) => {
                    sellersSchema.updateOne({ _id: decoded.userId }, { $set: req.body }, (err, profil) => {
                        if (err) throw err
                        res.send("Shipping details updated!")
                    })
                })
            }
        }
        else res.send("Field missing")
    })
})

app.put('/api/modifymyitem/:itemId', (req, res, next) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        let updatedItem = {}
        if (req.body.formData.name) updatedItem.name = req.body.formData.name
        if (req.body.formData.description) updatedItem.description = req.body.formData.description
        if (req.body.formData.price) updatedItem.price = req.body.formData.price
        updatedItem.notif = req.body.formData.notif
        if (req.body.formData.img) {
            let ext = req.body.formData.img.split(';')[0].match(/jpeg|png|gif/)[0]
            let data = req.body.formData.img.replace(/^data:image\/\w+;base64,/, "")
            let buffer = new Buffer.from(data, 'base64')
            let timestamp = new Date().getTime().toString()
            let path = "itemsPictures/" + decoded.userId + "/imgs/" + timestamp + "." + ext
            await mkdirp("public/itemsPictures/" + decoded.userId + "/imgs/")
            await sharp(buffer)
                .rotate()
                .resize(300, 306)
                .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                .jpeg({ quality: 10 })
                .png({ compressionLevel: 9 })
                .toFile('public/' + path)
            updatedItem.img = `${process.env.ORIGIN}/${path}`
        }
        if (req.body.formData.imgs.length >= 1 || req.body.formData.pictures.length >= 1) updatedItem.pictures = []
        if (req.body.formData.imgs.length >= 1) updatedItem.pictures = await req.body.formData.imgs
        if (req.body.formData.pictures.length >= 1) {
            const produit = await produitsSchema.findOne({ _id: req.body.itemId })
            if (produit.pictures.length >= 8 || (req.body.formData.pictures.length + produit.pictures.length > 8)) {
                res.json('Pictures limit reached')
                return next('Pictures limit reached')
            }
            else {
                if (req.body.formData.pictures.length >= 1) {
                    await forEach(req.body.formData.pictures, async pictures => {
                        let ext = pictures.split(';')[0].match(/jpeg|png/)
                        if (ext) {
                            let data = pictures.replace(/^data:image\/\w+;base64,/, "")
                            let buffer = new Buffer.from(data, 'base64')
                            let timestamp = new Date().getTime().toString()
                            let randomInt = Math.floor((Math.random() * 100) + 1)
                            let path = "itemsPictures/" + decoded.userId + "/imgs/" + timestamp + randomInt + "." + ext
                            let pathMin = "itemsPictures/" + decoded.userId + "/imgs/min/" + timestamp + randomInt + "-min." + ext
                            await mkdirp("public/itemsPictures/" + decoded.userId + "/imgs/min/")
                            const sharpPictures = await sharp(buffer)
                                .withMetadata()
                                .rotate()
                                .resize(800, 1000, { fit: "inside" })
                                .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                .jpeg({ quality: 10 })
                                .png({ compressionLevel: 9 })
                                .toFile('public/' + path)
                            await sharp(buffer)
                                .rotate()
                                .resize(300, 306)
                                .overlayWith('public/images/untitled.png', { gravity: sharp.gravity.southeast })
                                .jpeg({ quality: 10 })
                                .png({ compressionLevel: 9 })
                                .toFile('public/' + pathMin)
                            updatedItem.pictures.push({ src: `${process.env.ORIGIN}/${path}`, thumbnail: `${process.env.ORIGIN}/${pathMin}`, w: sharpPictures.width, h: sharpPictures.height })
                        }
                        else {
                            errorExtProfilPicture = true
                        }
                    })
                }
            }
        }
        produitsSchema.updateOne({ _id: req.params.itemId }, { $set: updatedItem }, (err, produit) => {
            if (err) throw err
            res.send("Item updated!")
        })
    })
})

app.put('/api/modifymyitempictures/:itemId', (req, res, next) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        let updatedItem = {}
        if (req.body.formData.name) updatedItem.name = req.body.formData.name
        if (req.body.formData.description) updatedItem.description = req.body.formData.description
        if (req.body.formData.price) updatedItem.price = req.body.formData.price
        produitsPicturesModel.updateOne({ _id: req.params.itemId }, { $set: updatedItem }, (err, produit) => {
            if (err) throw err
            res.send("Item updated!")
        })
    })
})

app.put('/api/modifymyitemvideos/:itemId', (req, res, next) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async (err, decoded) => {
        let updatedItem = {}
        if (req.body.formData.name) updatedItem.name = req.body.formData.name
        if (req.body.formData.description) updatedItem.description = req.body.formData.description
        if (req.body.formData.price) updatedItem.price = req.body.formData.price
        produitsVideosModel.updateOne({ _id: req.params.itemId }, { $set: updatedItem }, (err, produit) => {
            if (err) throw err
            res.send("Item updated!")
        })
    })
})

app.get('/api/getnotifications', async (req, res) => {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', async function (err, decoded) {
        const notifications = await notificationsSchema.find({ userId: decoded.userId })
        const notificationsItemBought = await notificationsSchema.find({ 'itemBoughtData.sellerId': decoded.userId, notifType: { $in: ["itemBought", "itemPicturesBought", "itemVideosBought"] } })
        if (notificationsItemBought) {
            var buyerIds = notificationsItemBought.map(notification => notification.itemBoughtData.buyerId)
            const produitsIds = notificationsItemBought.map(produit => produit.itemBoughtData.itemId).reduce((result, itemOn) => {
                return [...result, ...itemOn]
            }, [])
            const buyersData = await buyersSchema.find({ '_id': { $in: buyerIds } }).select('-password -status -shoppingbag')
            const sellersData = await sellersSchema.find({ '_id': { $in: buyerIds } }).select('-password -status -shoppingbag')
            let buyers = buyersData.concat(sellersData)
            const produits = await produitsSchema.find({ '_id': { $in: produitsIds }, 'bought': true })
            const produitsPictures = await produitsPicturesModel.find({ '_id': { $in: produitsIds }, 'boughtBy': { $exists: true, $not: { $size: 0 } } })
            const produitsVideos = await produitsVideosModel.find({ '_id': { $in: produitsIds }, 'boughtBy': { $exists: true, $not: { $size: 0 } } })
            const directSellProducts = await directSellProductsModel.find({ '_id': { $in: produitsIds } })
            res.json({ notifs: notifications, buyers: buyers, produits: produits, produitsPictures: produitsPictures, produitsVideos: produitsVideos, directSellProducts: directSellProducts })
        }
        else {
            res.send('No data')
        }
    })
})

app.post('/api/backconnection', function (req, res) {
    if (!req.body.username || !req.body.password) {
        res.send('A required field is missing')
    }
    else {
        req.body.username = req.body.username.trim()
        req.body.password = req.body.password.trim()
        try {
            staffModel.findOne({ username: req.body.username })
                .select('password')
                .then((member) => {
                    if (member) {
                        bcrypt.compare(req.body.password, member.password).then(result => {
                            if (result == false) {
                                res.send('Wrong password')
                            }
                            else {
                                jwt.sign({ userId: member._id }, 'staffsupersecret4.0', async (err, token) => {
                                    if (err) throw err
                                    await staffModel.updateOne({ _id: member._id }, { $set: { lastConnection: Date.now() } })
                                    res.cookie('staffauthtoken', token, { maxAge: 3600000 }).send('Ok')
                                })
                            }
                        })
                    }
                    else {
                        res.send('No account founded')
                    }
                })
        } catch (error) {
            throw error
        }
    }
})

app.post('/api/addstaff', function (req, res) {
    if (!req.body.username || !req.body.password) {
        res.send('A required field is missing')
    }
    req.body.username = req.body.username.trim()
    req.body.password = req.body.password.trim()
    try {
        staffModel.findOne({ username: req.body.username })
            .select('username password')
            .then((member) => {
                if (!member) {
                    bcrypt.hash(req.body.password, 10, function (err, hash) {
                        req.body.password = hash
                        let newMember = new staffModel(req.body)
                        newMember.save()
                            .then(async newMemberData => {
                                const token = await jwt.sign({ userId: newMemberData._id }, 'staffsupersecret4.0')
                                res.cookie('staffauthtoken', token).send('Ok')
                            })
                            .catch(err => { throw err })
                    })
                }
                else {
                    res.send("Username already taken")
                }
            })
    } catch (error) {
        throw error
    }
})

app.get('/api/authtoken', function (req, res) {
    jwt.verify(req.cookies.authtoken, 'usersupersecret4.0', function (err, decoded) {
        res.json({ token: decoded, status: true })
    })
})

app.get('/api/staffauthtoken', function (req, res) {
    jwt.verify(req.cookies.staffauthtoken, 'staffsupersecret4.0', function (err, decoded) {
        res.json({ token: decoded, status: true })
    })
})

mongoose.connect(url,
    {
        "auth": {
            "authSource": "admin"
        },
        "user": "Angusalex",
        "pass": "ConnectedUniverse92"
    },
    function (err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err)
        } else {
            console.log('Connection established to', url)
        }
    })

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname + '/statics/build/index.html'))
})

const port = process.env.PORT || 80

server.listen(port, (req, res) => {
    console.log(`Started process ${pid} on port ${port}`)
})