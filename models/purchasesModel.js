const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  sellersId: [String],
  itemsId: [String],
  date: {
    type: Date,
    default: Date.now
  },
  buyerId: String
})

const model = mongoose.model('Purchases', schema)

module.exports = model