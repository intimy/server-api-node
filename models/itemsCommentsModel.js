const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  userId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  username: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  itemId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  comment: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  userIp: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
})

const model = mongoose.model('ItemComments', schema)

module.exports = model