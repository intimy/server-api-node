const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  sellerId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  name: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  description: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  img: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  pictures: [{
    src: String,
    thumbnail: String,
    w: Number,
    h: Number
  }],
  price: {
    type: mongoose.Schema.Types.Number,
    required: true
  },
  shippingOption: {
    type: String,
    required: true
  },
  labelId: String,
  shippingLabelLink: String,
  directSell: {
    type: mongoose.Schema.Types.Boolean,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  bought: {
    type: Boolean,
    default: false
  },
  boughtOn: Date,
  buyerId: String
})

const model = mongoose.model('Produit', schema)

module.exports = model