const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
    emailAddress: {
        type: String,
        required: true
    },
    id: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

const model = mongoose.model('EmailList', schema)

module.exports = model