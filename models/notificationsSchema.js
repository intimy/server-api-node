const mongoose = require('mongoose')

let Schema = mongoose.Schema

const verifId = new Schema({
  stateVerification: Boolean,
  reason: String
})

const ItemBoughtData = new Schema({
    itemId: Array,
    buyerId: String,
    sellerId: String,
    shippingLabelLink: String
})

const ItemPicturesBoughtData = new Schema({
  itemId: Array,
  buyerId: String,
  sellerId: String,
})

const ItemVideosBoughtData = new Schema({
  itemId: Array,
  buyerId: String,
  sellerId: String,
})

const withdrawRequest = new Schema({
  validated: Boolean,
  reason: String
})

const schema = new Schema({
  //Commons Attributes
  notifType: {
    type: String,
    required: true
  },
  seen: {
    type: Boolean,
    required: true,
    default: false
  },
  userId: String,
  date: {
    type: Date,
    default: Date.now
  },
  //Notif types data
  verifId: verifId,
  itemBoughtData: ItemBoughtData,
  itemPicturesBoughtData: ItemPicturesBoughtData,
  itemVideosBoughtData: ItemVideosBoughtData,
  withdrawRequest: withdrawRequest
})

const model = mongoose.model('Notification', schema)

module.exports = model