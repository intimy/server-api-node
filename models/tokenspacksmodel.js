const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  quantity: {
    type: String,
    required: true
  },
  price: {
    type:  String,
    required: true
  }
})

const model = mongoose.model('Tokenspacks', schema)

module.exports = model