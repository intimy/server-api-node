
const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  sellerId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  name: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  description: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  img: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  videos: String,
  numberVideos: String,
  price: {
    type: mongoose.Schema.Types.Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  boughtBy: [{
    type: String,
    default: false
  }],
  deleted: Boolean,
  lastSale: Date,
  buyerId: String
})

const model = mongoose.model('ProduitsVideo', schema)

module.exports = model