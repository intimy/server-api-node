const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  sellerId: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  name: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  img: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  price: {
    type: mongoose.Schema.Types.Number,
    required: true
  },
  shippingOption: {
    type: String,
    required: true
  },
  labelId: String,
  country: {
    type: String,
    required: true
  },
  notif: {
    type: mongoose.Schema.Types.Boolean,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  bought: {
    type: Boolean,
    default: false
  },
  boughtOn: Date,
  buyerId: String
})

const model = mongoose.model('DirectSellProducts', schema)

module.exports = model