const mongoose = require('mongoose')

let Schema = mongoose.Schema

const AboutMe = new Schema({
  age: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  origins: String,
  bio: String,
  hairs: String,
  height: Number,
  eyes: String,
  status: String,
  interestedIn: [String],
  measurements: String,
  tattoos: String,
  piercings: String,
  whatILike: String,
  whatIDontLike: String
})

const EmailPreferences = new Schema({
  itemPurchased: {
    type: Boolean,
    default: true
  },
  itemCommentPosted: {
    type: Boolean,
    default: true
  }
})

const schema = new Schema({
  status: {
    type: String,
    required: true
  },
  stripeUserId: String,
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  profilPicture: String,
  pictures: [{
    src: String,
    thumbnail: String,
    w: Number,
    h: Number,
    kind: String
  }],
  videos: [{
    src: String,
    thumbnail: String
  }],
  password: {
    type: String,
    required: true
  },
  facebook: String,
  twitter: String,
  instagram: String,
  chaturbate: String,
  cam4: String,
  website: String,
  firstname: String,
  lastname: String,
  streetaddress: String,
  apt: String,
  city: String,
  state: String,
  zipcode: String,
  country: String,
  shoppingbag: [String],
  aboutMe: AboutMe,
  emailPreferences: EmailPreferences,
  live: Boolean,
  tokens: {
    type: Number,
    default: 0
  },
  customerId: String,
  verifiedUser: {
    type: Boolean,
    default: false
  },
  emailVerified: {
    type: Boolean,
    default: false
  },
  idVerif: {
    idPhoto: String,
    idFace: String
  },
  role: {
    type: Number,
    default: 0
  },
  lastConnection: {
    type: Date,
    default: Date.now
  },
  lastActivity: Date,
  date: {
    type: Date,
    default: Date.now
  }
}, { usePushEach: true })

const model = mongoose.model('Seller', schema)

module.exports = model