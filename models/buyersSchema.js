const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
  status: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  email: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  username: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  password: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  age: {
    type: mongoose.Schema.Types.String,
    required: true
  },
  profilPicture: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  pictures: [String],
  firstname: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  lastname: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  streetaddress: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  apt: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  city: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  state: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  zipcode: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  country: {
    type: mongoose.Schema.Types.String,
    required: false
  },
  emailVerified: {
    type: Boolean,
    default: false
  },
  tokens: Number,
  customerId: String,
  shoppingbag: [{
    type: mongoose.Schema.Types.String,
    required: false
  }],
  date: {
    type: Date,
    default: Date.now
  }
}, {usePushEach: true})

const model = mongoose.model('Buyer', schema)

module.exports = model