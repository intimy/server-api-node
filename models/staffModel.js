const mongoose = require('mongoose')

let Schema = mongoose.Schema

const schema = new Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    lastConnection: {
        type: Date,
        default: Date.now
    },
    date: {
        type: Date,
        default: Date.now
    }
}, { usePushEach: true })

const model = mongoose.model('Staff', schema)

module.exports = model